SET FOREIGN_KEY_CHECKS = 0; 

TRUNCATE `registrationapplications`;
TRUNCATE `result`;
TRUNCATE `resulterrors`;
TRUNCATE `studentgroupnames`;
TRUNCATE `studentgroups`;
TRUNCATE `task`;
TRUNCATE `tasklist`;
TRUNCATE `taskresult`;
TRUNCATE `tasksolver`;
TRUNCATE `tasktheme`;
TRUNCATE `test`;
TRUNCATE `testlist`;
TRUNCATE `testresults`;
TRUNCATE `teststatistics`;
TRUNCATE `testtype`;
TRUNCATE `testunmarked`;
TRUNCATE `user`;
TRUNCATE `usertype`;

SET FOREIGN_KEY_CHECKS = 1; 