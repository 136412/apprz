-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Май 31 2017 г., 21:29
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `apprz`
--

-- --------------------------------------------------------

--
-- Структура таблицы `registrationapplications`
--

CREATE TABLE IF NOT EXISTS `registrationapplications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) NOT NULL,
  `mail` varchar(256) NOT NULL,
  `password` varchar(1024) NOT NULL,
  `universityName` varchar(2048) NOT NULL,
  `contacts` varchar(1024) NOT NULL,
  `appDate` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `result`
--

CREATE TABLE IF NOT EXISTS `result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mark` float NOT NULL DEFAULT '0',
  `userId` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `resulterrors`
--

CREATE TABLE IF NOT EXISTS `resulterrors` (
  `step` int(11) NOT NULL COMMENT 'Error in this step occured',
  `taskResultId` int(11) NOT NULL,
  KEY `taskResultId` (`taskResultId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `studentgroupnames`
--

CREATE TABLE IF NOT EXISTS `studentgroupnames` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `studentgroups`
--

CREATE TABLE IF NOT EXISTS `studentgroups` (
  `studentId` int(11) NOT NULL,
  `nameId` int(11) NOT NULL,
  KEY `studentId` (`studentId`),
  KEY `name` (`nameId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `task`
--

CREATE TABLE IF NOT EXISTS `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `themeId` int(11) NOT NULL,
  `condition` varchar(2048) NOT NULL DEFAULT '',
  `difficulty` int(11) NOT NULL DEFAULT '0',
  `answer` varchar(1024) NOT NULL DEFAULT '',
  `name` varchar(128) NOT NULL DEFAULT '',
  `image` varchar(256) NOT NULL DEFAULT '',
  `authorId` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `theme` (`themeId`),
  KEY `author` (`authorId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tasklist`
--

CREATE TABLE IF NOT EXISTS `tasklist` (
  `taskId` int(11) NOT NULL,
  `testId` int(11) NOT NULL,
  KEY `taskId` (`taskId`),
  KEY `testId` (`testId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `taskresult`
--

CREATE TABLE IF NOT EXISTS `taskresult` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskId` int(11) NOT NULL,
  `resultId` int(11) NOT NULL,
  `answer` varchar(4096) NOT NULL,
  PRIMARY KEY (`taskId`,`resultId`),
  KEY `id` (`id`),
  KEY `resultId` (`resultId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `tasksolver`
--

CREATE TABLE IF NOT EXISTS `tasksolver` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(1024) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `tasksolver`
--

INSERT INTO `tasksolver` (`id`, `path`) VALUES
(1, 'pathToAlg'),
(2, 'pathToArithmetics'),
(3, 'pathToComb'),
(4, 'pathToSet');

-- --------------------------------------------------------

--
-- Структура таблицы `tasktheme`
--

CREATE TABLE IF NOT EXISTS `tasktheme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(1024) NOT NULL DEFAULT '',
  `solverId` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `solver` (`solverId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `tasktheme`
--

INSERT INTO `tasktheme` (`id`, `name`, `solverId`) VALUES
(1, 'Алгебра', 1),
(2, 'Арифметика', 2),
(3, 'Комбинаторика', 3),
(4, 'Теория множеств', 4);

-- --------------------------------------------------------

--
-- Структура таблицы `test`
--

CREATE TABLE IF NOT EXISTS `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `typeId` int(11) NOT NULL,
  `mark` float NOT NULL DEFAULT '0',
  `numOfMarks` int(11) NOT NULL DEFAULT '0',
  `timeConsumed` time NOT NULL DEFAULT '00:00:00',
  `authorId` int(11) NOT NULL,
  UNIQUE KEY `id` (`id`),
  KEY `author` (`authorId`),
  KEY `typeId` (`typeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `testlist`
--

CREATE TABLE IF NOT EXISTS `testlist` (
  `studentId` int(11) NOT NULL,
  `testId` int(11) NOT NULL,
  KEY `studentId` (`studentId`),
  KEY `testId` (`testId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `testresults`
--

CREATE TABLE IF NOT EXISTS `testresults` (
  `resultId` int(11) NOT NULL,
  `testId` int(11) NOT NULL,
  KEY `resultId` (`resultId`),
  KEY `testId` (`testId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `teststatistics`
--

CREATE TABLE IF NOT EXISTS `teststatistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `min` float NOT NULL,
  `max` float NOT NULL,
  `average` float NOT NULL,
  `numOfAttempts` int(11) NOT NULL,
  `testId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `testId` (`testId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `testtype`
--

CREATE TABLE IF NOT EXISTS `testtype` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `testtype`
--

INSERT INTO `testtype` (`id`, `name`) VALUES
(1, 'учебный тест'),
(2, 'контрольная работа');

-- --------------------------------------------------------

--
-- Структура таблицы `testunmarked`
--

CREATE TABLE IF NOT EXISTS `testunmarked` (
  `testId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  KEY `testId` (`testId`),
  KEY `userId` (`userId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fullname` varchar(1024) NOT NULL DEFAULT '' COMMENT 'Separated with ,',
  `mail` varchar(256) NOT NULL DEFAULT '',
  `password` varchar(1024) NOT NULL DEFAULT '',
  `typeId` int(11) NOT NULL,
  `creationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  UNIQUE KEY `id` (`id`),
  KEY `type` (`typeId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `fullname`, `mail`, `password`, `typeId`, `creationTime`) VALUES
(1, 'Админов Админ Админович', 'admin', 'admin', 0, '2017-05-31 17:37:21'),
(2, 'Профессоров Профессор Профессорович', 'pro', 'pro', 1, '2017-05-31 17:37:21'),
(3, 'Студентов Студент Студентович', 'stu', 'stu', 2, '2017-05-31 17:38:02');

-- --------------------------------------------------------

--
-- Структура таблицы `usertype`
--

CREATE TABLE IF NOT EXISTS `usertype` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `usertype`
--

INSERT INTO `usertype` (`id`, `name`) VALUES
(0, 'админ'),
(1, 'преподаватель'),
(2, 'студент');

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `result`
--
ALTER TABLE `result`
  ADD CONSTRAINT `result_ibfk_3` FOREIGN KEY (`userId`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `resulterrors`
--
ALTER TABLE `resulterrors`
  ADD CONSTRAINT `resulterrors_ibfk_1` FOREIGN KEY (`taskResultId`) REFERENCES `taskresult` (`id`);

--
-- Ограничения внешнего ключа таблицы `studentgroups`
--
ALTER TABLE `studentgroups`
  ADD CONSTRAINT `studentgroups_ibfk_1` FOREIGN KEY (`studentId`) REFERENCES `user` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `studentgroups_ibfk_2` FOREIGN KEY (`nameId`) REFERENCES `studentgroupnames` (`id`);

--
-- Ограничения внешнего ключа таблицы `task`
--
ALTER TABLE `task`
  ADD CONSTRAINT `task_ibfk_2` FOREIGN KEY (`authorId`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `task_ibfk_3` FOREIGN KEY (`themeId`) REFERENCES `tasktheme` (`id`);

--
-- Ограничения внешнего ключа таблицы `tasklist`
--
ALTER TABLE `tasklist`
  ADD CONSTRAINT `tasklist_ibfk_1` FOREIGN KEY (`taskId`) REFERENCES `task` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tasklist_ibfk_2` FOREIGN KEY (`testId`) REFERENCES `test` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `taskresult`
--
ALTER TABLE `taskresult`
  ADD CONSTRAINT `taskresult_ibfk_1` FOREIGN KEY (`taskId`) REFERENCES `task` (`id`),
  ADD CONSTRAINT `taskresult_ibfk_2` FOREIGN KEY (`resultId`) REFERENCES `result` (`id`);

--
-- Ограничения внешнего ключа таблицы `tasktheme`
--
ALTER TABLE `tasktheme`
  ADD CONSTRAINT `tasktheme_ibfk_1` FOREIGN KEY (`solverId`) REFERENCES `tasksolver` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `test`
--
ALTER TABLE `test`
  ADD CONSTRAINT `test_ibfk_3` FOREIGN KEY (`authorId`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `test_ibfk_5` FOREIGN KEY (`typeId`) REFERENCES `testtype` (`id`);

--
-- Ограничения внешнего ключа таблицы `testlist`
--
ALTER TABLE `testlist`
  ADD CONSTRAINT `testlist_ibfk_1` FOREIGN KEY (`studentId`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `testlist_ibfk_2` FOREIGN KEY (`testId`) REFERENCES `test` (`id`);

--
-- Ограничения внешнего ключа таблицы `testresults`
--
ALTER TABLE `testresults`
  ADD CONSTRAINT `testresults_ibfk_1` FOREIGN KEY (`resultId`) REFERENCES `result` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `testresults_ibfk_2` FOREIGN KEY (`testId`) REFERENCES `test` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `teststatistics`
--
ALTER TABLE `teststatistics`
  ADD CONSTRAINT `teststatistics_ibfk_3` FOREIGN KEY (`testId`) REFERENCES `test` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `testunmarked`
--
ALTER TABLE `testunmarked`
  ADD CONSTRAINT `testunmarked_ibfk_1` FOREIGN KEY (`testId`) REFERENCES `test` (`id`),
  ADD CONSTRAINT `testunmarked_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`typeId`) REFERENCES `usertype` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
