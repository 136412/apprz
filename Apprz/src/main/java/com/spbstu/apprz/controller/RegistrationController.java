package com.spbstu.apprz.controller;

import com.spbstu.apprz.model.RegistrationApplication;
import com.spbstu.apprz.model.User;
import com.spbstu.apprz.repository.RegistrationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class RegistrationController {

    @Autowired
    private RegistrationRepository registrationRepository;

    @RequestMapping(value = "/registration_form", method = RequestMethod.GET)
    public String registrationWelcome(Model model) {
        return "registration_form";
    }

    @RequestMapping(value = "/registration_form", method = RequestMethod.POST, params = {"loginidSt", "emailSt", "loginpswSt"})
    public ModelMap registrateStudent(@RequestParam("loginidSt") String login, @RequestParam("emailSt") String mail,
                                      @RequestParam("loginpswSt") String pass, @RequestParam(value = "tabID") String tabID,
                                      ModelMap model) {
        String message = "exist";
        User user = new User(login, mail, pass);

        if (!registrationRepository.checkIfUserExist(user, false)) {
            registrationRepository.insertStudent(user);
            message = "created";
        }

        model.addAttribute("tabID", tabID);
        model.addAttribute("userSt", message);
        return model;
    }

    @RequestMapping(value = "/registration_form", method = RequestMethod.POST,
                    params = {"loginidPr", "emailPr", "loginpswPr", "universityPr", "contactsPr"})
    public ModelMap registrateProfessor(@RequestParam("loginidPr") String login, @RequestParam("emailPr") String mail,
                                        @RequestParam("loginpswPr") String pass, @RequestParam("universityPr") String uni,
                                        @RequestParam("contactsPr") String contacts, @RequestParam(value = "tabID") String tabID,
                                        ModelMap model) {
        String message = "exist";

        if (!registrationRepository.checkIfUserExist(new User(login, mail, pass), true)) {
            registrationRepository.insertProfessor(new RegistrationApplication(login, mail, pass, uni, contacts));
            message = "created";
        }

        model.addAttribute("tabID", tabID);
        model.addAttribute("userPr", message);
        return model;
    }
}
