package com.spbstu.apprz.controller;

import com.spbstu.apprz.model.Task;
import com.spbstu.apprz.model.Test;
import com.spbstu.apprz.repository.ProfessorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.Time;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Controller
public class ProfessorController {

    @Autowired
    private ProfessorRepository professorRepository;

    private String professorName;
    private String tabId;
    private int professorId;

    private void fillData(ModelMap modelMap) {
        modelMap.addAttribute("tabID", this.tabId);
        modelMap.addAttribute("proff_id", this.professorId);
        modelMap.addAttribute("proff_name", this.professorName);
        modelMap.addAttribute("myTests", professorRepository.selectProfessorTestListByProfessorId(this.professorId));
        modelMap.addAttribute("usersProff", professorRepository.selectAllUserList());
        modelMap.addAttribute("tasks", professorRepository.selectTaskList());
        modelMap.addAttribute("testTaskResults", professorRepository.selectStudentResultsByProfessorId(this.professorId));
        modelMap.addAttribute("statisticsTests", professorRepository.selectTestStatisticsByProfessorId(this.professorId));
        modelMap.addAttribute("themes", professorRepository.selectTaskThemeList());
        modelMap.addAttribute("groups", professorRepository.selectStudentGroupNameList());
    }

    @RequestMapping(value = {"/professor_form"}, method = RequestMethod.GET, params = "userName")
    public ModelAndView professorWelcome(@RequestParam(value = "userName") String professorName,
                                         @RequestParam(value = "userId") int professorId,
                                         @RequestParam(value = "tabID", required = false, defaultValue = "1a") String tabId,
                                         ModelMap model, ModelAndView modelAndView) {
        try {
            this.professorName = URLDecoder.decode(professorName, "UTF-8");
            this.professorId = professorId;
            this.tabId = tabId;
        } catch (Exception ex) {
            return modelAndView;
        }

        return new ModelAndView("redirect:professor_form", new ModelMap());
    }

    @RequestMapping(value = {"/professor_form"}, method = RequestMethod.GET, params = "!userName")
    public ModelAndView professorForm(ModelMap model, ModelAndView modelAndView) {
        fillData(model);

        return new ModelAndView("professor_form", model);
    }


    @RequestMapping(value = {"/professor_form"}, method = RequestMethod.POST, params = {"taskname", "taskDef", "taskanswer"})
    public ModelMap createTask(@RequestParam(value = "taskname") String taskName,
                               @RequestParam(value = "taskDef") String taskDef,
                               @RequestParam(value = "selectedtaskdiff") int taskDifficulty,
                               @RequestParam(value = "selectedtasktheme") int taskThemeId,
                               @RequestParam(value = "taskanswer") String taskAnswer,
                               ModelMap model) {
        if (professorRepository.checkTaskByName(taskName)) {
            model.addAttribute("taskCreated", "false");
        } else {
            Task task = new Task(taskName, taskThemeId, taskDef, taskDifficulty, taskAnswer, this.professorId);

            professorRepository.insertTask(task);

            model.addAttribute("taskCreated", "true");
        }
        this.tabId = "1a";
        fillData(model);

        return model;
    }

    @RequestMapping(value = {"/professor_form"}, method = RequestMethod.POST, params = {"testId", "studentlist", "grouplist"})
    public ModelMap appointTest(@RequestParam(value = "testId") int testId,
                                @RequestParam(value = "studentlist") String studentIdListRaw,
                                @RequestParam(value = "grouplist") String groupIdListRaw,
                                ModelMap model) {
        List<Integer> groupIdList = Pattern.compile(",")
                .splitAsStream(groupIdListRaw)
                .map(Integer::valueOf)
                .collect(Collectors.toList());
        Set<Integer> studentIdSet = Pattern.compile(",")
                        .splitAsStream(studentIdListRaw)
                        .map(Integer::valueOf)
                        .collect(Collectors.toSet());

        groupIdList.forEach((id) -> studentIdSet.addAll(professorRepository.selectUserIdListByGroupId(id)));
        studentIdSet.forEach((id) -> professorRepository.insertTestToTestList(id, testId));
        this.tabId = "1a";
        fillData(model);

        return model;
    }

    @RequestMapping(value = {"/professor_form"}, method = RequestMethod.POST, params = {"tabId", "testId", "myTests"})
    public ModelMap getMyTestTaskDetails(@RequestParam(value = "tabId") String tabId,
                                         @RequestParam(value = "testId") int testId,
                                         ModelMap model) {
        Test test = professorRepository.selectTestDataByTestId(testId);

        this.tabId = tabId;
        model.addAttribute("tasksInTest", professorRepository.selectTaskListInTestByTestId(testId));
        model.addAttribute("gTestName", test.getName());
        model.addAttribute("gTestTheme", test.getType());
        model.addAttribute("gTestAuthorId", test.getAuthorId());
        model.addAttribute("gTestId", testId);
        fillData(model);

        return model;
    }

    @RequestMapping(value = {"/professor_form"}, method = RequestMethod.POST, params = {"tabId", "taskId", "testTasks"})
    public ModelMap getTestTaskDetails(@RequestParam(value = "tabId") String tabId,
                                       @RequestParam(value = "taskId") int taskId,
                                       ModelMap model) {
        this.tabId = tabId;
        model.addAttribute("gTask", professorRepository.selectTaskByTaskId(taskId));
        fillData(model);

        return model;
    }

    @RequestMapping(value = {"/professor_form"}, method = RequestMethod.POST, params = {"tabId", "testId", "testName", "testDetailedInfo"})
    public ModelMap getTestDetailedInfo(@RequestParam(value = "tabId") String tabId,
                                        @RequestParam(value = "testName") String testName,
                                        @RequestParam(value = "testId") int testId,
                                        ModelMap model) {
        this.tabId = tabId;
        model.addAttribute("testStatsTasks", professorRepository.selectTestDetailedStatisticsByTestId(testId));
        model.addAttribute("testName", testName);
        fillData(model);

        return model;
    }

    @RequestMapping(value = {"/professor_form"}, method = RequestMethod.POST, params = {"testId", "deleteTest"})
    public ModelMap deleteTest(@RequestParam(value = "testId") int testId,
                               ModelMap model) {
        this.tabId = "1a";
        professorRepository.deleteTest(testId);
        model.addAttribute("testWasDeleted", "true");
        fillData(model);

        return model;
    }

    @RequestMapping(value = {"/professor_form"}, method = RequestMethod.POST, params = {"taskId", "deleteTask"})
    public ModelMap deleteTask(@RequestParam(value = "taskId") int taskId,
                               ModelMap model) {
        if (!professorRepository.checkTaskInTestByTaskId(taskId)) {
            professorRepository.deleteTask(taskId);
            model.addAttribute("taskWasDeleted", "true");
        } else {
            model.addAttribute("taskWasDeleted", "false");
        }
        this.tabId = "4a";
        fillData(model);

        return model;
    }

    @RequestMapping(value = {"/professor_form"}, method = RequestMethod.POST, params = {"resultId"})
    public ModelAndView getTestSolution(@RequestParam(value = "resultId") int resultId,
                                        ModelMap model, ModelAndView modelAndView) {
        String urlName;
        try {
            urlName = URLEncoder.encode(this.professorName, "UTF-8");
        } catch (Exception ex) {
            return modelAndView;
        }

        model.addAttribute("masterId", this.professorId);
        model.addAttribute("userName", urlName);
        model.addAttribute("resultId", resultId);
        model.addAttribute("pathBack", "professor_form");

        return new ModelAndView("redirect:decision_result_form", model);
    }

    @RequestMapping(value = {"/professor_form"}, method = RequestMethod.POST, params = "testname")
    public ModelMap createTest(@RequestParam(value = "testname") String testName,
                               @RequestParam(value = "selectedtesttype") int testTypeId,
                               @RequestParam(value = "testtime", required = false, defaultValue = "0") int testTime,
                               @RequestParam(value = "testtaskidlist") String taskIdList,
                               ModelMap model) {
        if (professorRepository.checkTestByName(testName)) {
            model.addAttribute("testCreated", "false");
        } else {
            List<Integer> idList = Pattern.compile(",")
                    .splitAsStream(taskIdList)
                    .map(Integer::valueOf)
                    .collect(Collectors.toList());
            Test test = new Test(testName, testTypeId, this.professorId, new Time(testTime * 1000 * 60));

            professorRepository.insertTest(test, idList);

            model.addAttribute("testCreated", "true");
        }
        this.tabId = "1a";
        fillData(model);

        return model;
    }

    @RequestMapping(value = {"/professor_form"}, method = RequestMethod.POST, params = "groupname")
    public ModelMap createGroup(@RequestParam(value = "groupname") String groupName,
                                @RequestParam(value = "groupstudentidlist") String studentIdList,
                                ModelMap model) {
        if (professorRepository.checkStudentGroupNameByName(groupName)) {
            model.addAttribute("groupCreated", "false");
        } else {
            List<Integer> idList = Pattern.compile(",")
                    .splitAsStream(studentIdList)
                    .map(Integer::valueOf)
                    .collect(Collectors.toList());

            professorRepository.insertStudentGroup(groupName, idList);

            model.addAttribute("groupCreated", "true");
        }
        this.tabId = "1a";
        fillData(model);

        return model;
    }
}
