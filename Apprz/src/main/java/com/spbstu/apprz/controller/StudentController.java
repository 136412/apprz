package com.spbstu.apprz.controller;

import com.spbstu.apprz.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.net.URLDecoder;
import java.net.URLEncoder;

@Controller
public class StudentController {
    @Autowired
    private StudentRepository studentRepository;

    private int studentId;
    private String studentName;
    private String tabId;

    /* *****  MAIN STUDENT PAGE ***** */

    private void fillData(ModelMap model) {
        model.addAttribute("student_id", this.studentId);
        model.addAttribute("student_name", this.studentName);
        model.addAttribute("tabID", this.tabId);
        model.addAttribute("assignedTests", studentRepository.selectArrangedTestsByUserId(this.studentId));
        model.addAttribute("availableTests", studentRepository.selectAvailableTests(this.studentId));
        model.addAttribute("resultsOfTheTests", studentRepository.selectCheckedTestsByUserId(this.studentId));
        model.addAttribute("evaluateTests", studentRepository.selectUnmarkedTestsByUserId(this.studentId));
    }

    @RequestMapping(value = {"/student_form"}, method = RequestMethod.GET, params = {"userName"})
    public ModelAndView studentWelcome(@RequestParam(value = "userName") String studentName,
                                       @RequestParam(value = "userId") int studentId,
                                       @RequestParam(value = "tabID", defaultValue = "1a", required = false) String tabId,
                                       ModelMap model, ModelAndView modelAndView) {
        try {
            this.studentName = URLDecoder.decode(studentName, "UTF-8");
            this.studentId = studentId;
            this.tabId = tabId;
        } catch (Exception ex) {
            return modelAndView;
        }

        return new ModelAndView("redirect:student_form", new ModelMap());
    }

    @RequestMapping(value = {"/student_form"}, method = RequestMethod.GET, params = "!userName")
    public ModelAndView studentForm(ModelMap model, ModelAndView modelAndView) {
        fillData(model);

        return new ModelAndView("student_form", model);
    }

    @RequestMapping(value = {"/student_form"}, method = RequestMethod.POST, params = "beginTest")
    public ModelAndView beginTesting(@RequestParam(value = "testID") int testId,
                                     @RequestParam(value = "tabID") String tabId,
                                     @RequestParam(value = "timeConsumed") int timeConsumed,
                                     ModelMap model, ModelAndView modelAndView) {
        String urlName;
        try {
            urlName = URLEncoder.encode(this.studentName, "UTF-8");
        } catch (Exception ex) {
            return modelAndView;
        }
        this.tabId = tabId;
        model.addAttribute("studentId", this.studentId);
        model.addAttribute("studentName", urlName);
        model.addAttribute("timeConsumed", timeConsumed);
        model.addAttribute("testId", testId);

        return new ModelAndView("redirect:decision_form", model);
    }

    @RequestMapping(value = {"/student_form"}, method = RequestMethod.POST, params = "setMyTest")
    public ModelMap setTestToMe(@RequestParam(value = "testID") int testId,
                                @RequestParam(value = "tabID") String tabId,
                                ModelMap model) {
        this.tabId = tabId;
        studentRepository.insertTestIdToTestList(testId, this.studentId);
        fillData(model);

        return model;
    }

    @RequestMapping(value = {"/student_form"}, method = RequestMethod.POST, params = "seeSolution")
    public ModelAndView seeSolution(@RequestParam(value = "resultId") int resultId,
                                    @RequestParam(value = "tabID") String tabId,
                                    ModelMap model, ModelAndView modelAndView) {
        String urlName;
        try {
            urlName = URLEncoder.encode(this.studentName, "UTF-8");
        } catch (Exception ex) {
            return modelAndView;
        }

        this.tabId = tabId;
        model.addAttribute("masterId", this.studentId);
        model.addAttribute("userName", urlName);
        model.addAttribute("resultId", resultId);
        model.addAttribute("pathBack", "student_form");

        return new ModelAndView("redirect:decision_result_form", model);
    }

    @RequestMapping(value = {"/student_form"}, method = RequestMethod.POST, params = "setMark")
    public ModelMap setMark(@RequestParam(value = "testID") int testId,
                            @RequestParam(value = "tabID") String tabId,
                            @RequestParam(value = "mark") int mark,
                            ModelMap model) {
        this.tabId = tabId;
        studentRepository.insertTestMark(testId, this.studentId, mark);
        fillData(model);

        return model;
    }
}
