package com.spbstu.apprz.controller;


import com.spbstu.apprz.model.Task;
import com.spbstu.apprz.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Controller
public class DecisionController {
    @Autowired
    private StudentRepository studentRepository;

    private int studentId;
    private String studentName;
    private int testId;
    private int testLength;
    private List<Task> taskList;


    private void fillData(ModelMap model) {
        model.addAttribute("student_name", this.studentName);
        model.addAttribute("test_length", this.testLength);
        model.addAttribute("my_test", this.taskList);
    }

    @RequestMapping(value = {"/decision_form"}, method = RequestMethod.GET, params = "studentName")
    public ModelAndView decisionWelcome(@RequestParam(value = "studentName") String studentName,
                                        @RequestParam(value = "studentId") int studentId,
                                        @RequestParam(value = "testId") int testId,
                                        @RequestParam(value = "timeConsumed") int timeConsumed,
                                        ModelMap model, ModelAndView modelAndView) {
        try {
            this.studentId = studentId;
            this.studentName = URLDecoder.decode(studentName, "UTF-8");
            this.testId = testId;
            this.testLength = timeConsumed * 60 * 1000;
            this.taskList = studentRepository.selectTaskListByTestId(testId);
        } catch (Exception ex) {
            return modelAndView;
        }

        return new ModelAndView("redirect:decision_form", new ModelMap());
    }

    @RequestMapping(value = {"/decision_form"}, method = RequestMethod.GET, params = "!studentName")
    public ModelAndView decisionForm(ModelMap model, ModelAndView modelAndView) {
        fillData(model);

        return new ModelAndView("decision_form", model);
    }

    @RequestMapping(value = {"/decision_form"}, method = RequestMethod.POST)
    public ModelAndView getTestData(@RequestParam(value = "testData") String testData,
                                    ModelMap model, ModelAndView modelAndView) {
        if (!testData.isEmpty()) {
            List<String> resultList = Pattern.compile("@")
                    .splitAsStream(testData.replace("\r\n", ";").replace(";;", ";"))
                    .collect(Collectors.toList());

            studentRepository.insertTestResults(resultList, this.taskList, this.studentId, this.testId);
        }

        String urlName;
        try {
            urlName = URLEncoder.encode(this.studentName, "UTF-8");
        } catch (Exception ex) {
            return modelAndView;
        }

        model.addAttribute("userName", urlName);
        model.addAttribute("userId", this.studentId);

        return new ModelAndView("redirect:student_form", model);
    }

}
