package com.spbstu.apprz.controller;

import com.spbstu.apprz.repository.AdminRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.net.URLDecoder;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


@Controller
public class AdminController {
    @Autowired
    private AdminRepository adminRepository;

    private String adminName;
    private boolean wasAdded;
    private boolean wasDeleted;

    private void fillData(ModelMap model) {
        model.addAttribute("users", adminRepository.selectAllUsers());
        model.addAttribute("usersReg", adminRepository.selectProfessorRequests());
        model.addAttribute("usersDel", adminRepository.selectOldUsers());
        model.addAttribute("admin_name", this.adminName);
        model.addAttribute("wasAdded", this.wasAdded ? "true" : "false");
        model.addAttribute("wasDeleted", this.wasDeleted  ? "true" : "false");
    }


    @RequestMapping(value = {"/admin_form"}, method = RequestMethod.GET, params = "userName")
    public ModelAndView adminWelcome(@RequestParam(value = "userName") String adminName, ModelMap model, ModelAndView modelAndView) {
        try {
            this.adminName = URLDecoder.decode(adminName, "UTF-8");
        } catch (Exception ex) {
            return modelAndView;
        }

        return new ModelAndView("redirect:admin_form", new ModelMap());
    }

    @RequestMapping(value = {"/admin_form"}, method = RequestMethod.GET, params = "!userName")
    public ModelAndView adminForm(ModelMap model, ModelAndView modelAndView) {
        fillData(model);

        return new ModelAndView("admin_form", model);
    }

    @RequestMapping(value = {"/admin_form"}, method = RequestMethod.POST, params = "proffRegId")
    public ModelMap regProfessor(@RequestParam(value = "proffRegId") int proffId,
                                 @RequestParam(value = "tabID") String tabID, ModelMap model) {
        adminRepository.insertProfessor(adminRepository.selectProfById(proffId));

        wasAdded = true;
        fillData(model);
        model.addAttribute("tabID", tabID);
        wasAdded = false;

        return model;
    }


    @RequestMapping(value = {"/admin_form"}, method = RequestMethod.POST, params = "deletedUserValue")
    public ModelMap delUser(@RequestParam(value = "deletedUserValue") String userIds,
                            @RequestParam(value = "tabID") String tabID, ModelMap model) {
        List<Integer> idList = Pattern.compile(",")
                .splitAsStream(userIds)
                .map(Integer::valueOf)
                .collect(Collectors.toList());

        idList.forEach((id) -> adminRepository.deleteUserById(id));

        wasDeleted = true;
        fillData(model);
        model.addAttribute("tabID", tabID);
        wasDeleted = false;

        return model;
    }

}
