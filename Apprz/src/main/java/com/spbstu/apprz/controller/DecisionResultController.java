package com.spbstu.apprz.controller;


import com.spbstu.apprz.model.Task;
import com.spbstu.apprz.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

@Controller
public class DecisionResultController {
    @Autowired
    private StudentRepository studentRepository;

    private int masterId;
    private String userName;
    private int resultId;
    private String pathBack;
    private List<Task> taskList;
    private List<String> errorList;

    private void fillData(ModelMap model) {
        model.addAttribute("user_name", this.userName);
        model.addAttribute("my_test", this.taskList);
    }

    @RequestMapping(value = {"/decision_result_form"}, method = RequestMethod.GET, params = "userName")
    public ModelAndView decisionResultWelcome(@RequestParam(value = "masterId") int masterId,
                                              @RequestParam(value = "userName") String userName,
                                              @RequestParam(value = "resultId") int resultId,
                                              @RequestParam(value = "pathBack") String pathBack,
                                              ModelMap model, ModelAndView modelAndView) {
        try {
            this.masterId = masterId;
            this.userName = URLDecoder.decode(userName, "UTF-8");
            this.resultId = resultId;
            this.pathBack = pathBack;
            this.taskList = studentRepository.selectTestResultsByResultId(this.resultId);

            this.errorList = new ArrayList<>();
            this.errorList.add("1");
            this.errorList.add("2");
            this.errorList.add("3");
        } catch (Exception ex) {
            return modelAndView;
        }

        return new ModelAndView("redirect:decision_result_form", new ModelMap());
    }

    @RequestMapping(value = {"/decision_result_form"}, method = RequestMethod.GET, params = "!userName")
    public ModelAndView decisionResultForm(ModelMap model, ModelAndView modelAndView) {
        fillData(model);

        return new ModelAndView("decision_result_form", model);
    }

    @RequestMapping(value = {"/decision_result_form"}, method = RequestMethod.POST)
    public ModelAndView goBack(ModelMap model, ModelAndView modelAndView) {
        String urlName;
        try {
            urlName = URLEncoder.encode(this.userName, "UTF-8");
        } catch (Exception ex) {
            return modelAndView;
        }

        model.addAttribute("userName", urlName);
        model.addAttribute("userId", this.masterId);

        if (this.pathBack.equals("student_form")) {
            model.addAttribute("tabID", "3a");
        } else {
            model.addAttribute("tabID", "2a");
        }

        return new ModelAndView("redirect:" + pathBack, model);
    }
}
