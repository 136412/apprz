package com.spbstu.apprz.controller;

import com.spbstu.apprz.model.User;
import com.spbstu.apprz.repository.LoginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.net.URLEncoder;

@Controller
public class LoginController {

    @Autowired
    private LoginRepository loginRepository;


    @RequestMapping(value = {"/", "/index"}, method = RequestMethod.GET)
    public String welcome(Model model) {
        return "index";
    }

    @RequestMapping(value = {"/index"}, method = RequestMethod.POST)
    public ModelAndView login(@RequestParam(value = "loginid") String mail, @RequestParam(value = "loginpsw") String password,
                        ModelMap modelMap, ModelAndView modelAndView) {
        if (loginRepository.selectUserByMail(mail) == null) {
            modelAndView.addObject("user", "null");
            return modelAndView;
        }

        User user = loginRepository.selectUser(mail, password);
        if (user == null) {
            modelAndView.addObject("user", "badPass");
            return modelAndView;
        } else {
            String urlName;
            try {
                urlName = URLEncoder.encode(user.getFullname(), "UTF-8");
            } catch (Exception ex) {
                return modelAndView;
            }

            if (user.getType().equalsIgnoreCase("�������")) {
                modelMap.addAttribute("userName", urlName);
                modelMap.addAttribute("userId", user.getId());
                return new ModelAndView("redirect:student_form", modelMap);
            } else if (user.getType().equalsIgnoreCase("�����")) {
                modelMap.addAttribute("userName", urlName);
                return new ModelAndView("redirect:admin_form", modelMap);
            } else if (user.getType().equalsIgnoreCase("�������������")) {
                modelMap.addAttribute("userName", urlName);
                modelMap.addAttribute("userId", user.getId());
                return new ModelAndView("redirect:professor_form", modelMap);
            } else {
                // Impossible choice
                return modelAndView;
            }
        }


    }
}
