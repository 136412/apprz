package com.spbstu.apprz.repository;

import com.spbstu.apprz.model.Task;
import com.spbstu.apprz.model.Test;
import com.spbstu.apprz.model.TestResult;
import com.spbstu.apprz.model.TestStatistics;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Repository
public class StudentRepository extends CommonRepository {

    public List<Test> selectArrangedTestsByUserId(int id) {
        List<Test> arrangedTests;
        try {
            arrangedTests = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("studentSelArrangedTestListByStudentId"),
                    new Object[]{id},
                    (rs, rowNum) ->
                            new Test(rs.getInt("id"), rs.getString("name"), rs.getString("fullname"),
                                    rs.getString("typename"), rs.getTime("timeConsumed")));
        } catch (EmptyResultDataAccessException ex) {
            arrangedTests = null;
        }

        return arrangedTests;
    }


    public List<Test> selectAvailableTests(int id) {
        List<Test> availableTests;
        try {
            availableTests = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("studentSelUnarrangedTestListByStudentId"),
                    new Object[]{id},
                    (rs, rowNum) ->
                            new Test(rs.getInt("id"), rs.getString("name"), rs.getString("fullname"),
                                    rs.getString("typename")));
        } catch (EmptyResultDataAccessException ex) {
            availableTests = null;
        }

        return availableTests;
    }


    public List<TestResult> selectCheckedTestsByUserId(int id) {
        List<TestResult> checkedTests;
        try {
            checkedTests = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("studentSelTestResultByStudentId"),
                    new Object[]{id},
                    (rs, rowNum) ->
                            new TestResult(rs.getInt("testId"), rs.getString("name"), rs.getString("typeName"),
                                    rs.getFloat("mark"), rs.getString("fullname"), rs.getInt("resultId")));
        } catch (EmptyResultDataAccessException ex) {
            checkedTests = null;
        }

        return checkedTests;
    }

    public List<Test> selectUnmarkedTestsByUserId(int id) {
        List<Test> unmarkedTests;
        try {
            unmarkedTests = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("studentSelUnmarkedTestListByStudentId"),
                    new Object[]{id},
                    (rs, rowNum) ->
                            new Test(rs.getInt("id"), rs.getString("name"), rs.getString("fullname"),
                                    rs.getString("typename")));
        } catch (EmptyResultDataAccessException ex) {
            unmarkedTests = null;
        }

        return unmarkedTests;
    }

    public List<Task> selectTaskListByTestId(int id) {
        List<Task> taskList;
        try {
            taskList = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("studentSelTaskDataByTestId"),
                    new Object[]{id},
                    (rs, rowNum) ->
                            new Task(rs.getInt("id"), rs.getString("condition"), rs.getString("name")));
        } catch (EmptyResultDataAccessException ex) {
            taskList = null;
        }

        return taskList;
    }

    public List<Task> selectTestResultsByResultId(int resultId) {
        List<Task> taskList;

        try {
            taskList = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("studentSelTaskDataByResultId"),
                    new Object[]{resultId},
                    (rs, rowNum) ->
                            new Task(rs.getInt("id"), rs.getString("condition"), rs.getString("name"),
                                    rs.getString("answer"), rs.getInt("authorId")));

            for (int index = 0; index < taskList.size(); index++) {
                String currResult = taskList.get(index).getAnswer(),
                    currSolution = currResult.substring(0, currResult.lastIndexOf(";")),
                    currAnswer = currResult.substring(currSolution.length() + 1);
                taskList.get(index).setSolution(currSolution.replace(";", "\r\n"));
                taskList.get(index).setAnswer(currAnswer);
            }

        } catch (Exception ex) {
            taskList = null;
        }

        return taskList;
    }


    public boolean insertTestResults(List<String> resultDataList, List<Task> taskList,
                                     int userId, int testId) {
        GeneratedKeyHolder holder = new GeneratedKeyHolder();
        this.getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement statement = con.prepareStatement(getDatabaseQuery().getProperty("studentInsResultInfo"), Statement.RETURN_GENERATED_KEYS);
                statement.setInt(1, userId);
                return statement;
            }
        }, holder);

        int resId = holder.getKey().intValue();

        for (int index = 0; index < taskList.size(); index++) {
            this.getJdbcTemplate().update(getDatabaseQuery().getProperty("studentInsTaskAnswer"),
                    taskList.get(index).getId(), resId, resultDataList.get(index));
        }

        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("studentInsTestResult"),
                resId, testId);

        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("studentDelArrangedTest"),
                testId, userId);

        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("studentInsUnmarkedTest"),
                testId, userId);

        /********************/
        /* Update test stats */
        /********************/
        float mark = ((int)(Math.random() * 500)) / 100.0f;

        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("studentUpdResultByResultId"),
                mark, resId);
        updateTestStatsByTestId(testId, mark);

        /********************/
        /********************/
        /********************/

        return true;
    }

    public boolean updateTestStatsByTestId(int testId, float mark) {
        TestStatistics testStatistics;

        try {
            testStatistics = this.getJdbcTemplate().queryForObject(
                    getDatabaseQuery().getProperty("studentSelTestStatByTestId"),
                    new Object[]{testId},
                    (rs, rowNum) -> new TestStatistics(rs.getInt("id"), rs.getFloat("min"), rs.getFloat("max"),
                            rs.getFloat("average"), rs.getInt("testId"), rs.getInt("numOfAttempts")));
        } catch (EmptyResultDataAccessException ex) {
            testStatistics = null;
        }

        if (testStatistics == null) {
            this.getJdbcTemplate().update(getDatabaseQuery().getProperty("studentInsTestStat"),
                    mark, mark, mark, 1, testId);
        } else {
            this.getJdbcTemplate().update(getDatabaseQuery().getProperty("studentUpdTestStatByTestStatId"),
                    Math.min(mark, testStatistics.getMin()), Math.max(mark, testStatistics.getMax()),
                    (testStatistics.getAverage() * testStatistics.getNumOfAttempts() + mark) / (testStatistics.getNumOfAttempts() + 1),
                    testStatistics.getNumOfAttempts() + 1, testStatistics.getId());
        }

        return true;
    }

    public boolean insertTestMark(int testId, int studentId, int mark) {
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("studentUpdTestMarkByTestId"),
                mark, testId);

        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("studentDelUnmarkedTestByTestId"),
                testId, studentId);

        return true;
    }

    public boolean insertTestIdToTestList(int testId, int studentId) {
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("studentInsTestList"),
                studentId, testId);

        return true;
    }
}
