package com.spbstu.apprz.repository;

import com.spbstu.apprz.model.User;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;



@Repository
public class LoginRepository extends CommonRepository {
    public User selectUserByMail(String mail) {
        User user;
        try {
            user = this.getJdbcTemplate().queryForObject(
                    getDatabaseQuery().getProperty("regSelUserDataByMail"),
                    new Object[]{mail},
                    (rs, rowNum) ->
                            new User(rs.getInt("id"), rs.getString("fullname"), rs.getString("mail"), "", null, rs.getString("name")));
        } catch (EmptyResultDataAccessException ex) {
            user = null;
        }

        return user;
    }
}
