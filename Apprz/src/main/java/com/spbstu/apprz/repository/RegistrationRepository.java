package com.spbstu.apprz.repository;

import com.spbstu.apprz.model.RegistrationApplication;
import com.spbstu.apprz.model.User;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;


@Repository
public class RegistrationRepository extends CommonRepository {

    private int studTypeId = -1;

    public boolean checkIfUserExist(User user, boolean regAppCheck) {
        User foundUser = this.selectUser(user.getMail(), user.getPassword());
        RegistrationApplication application = null;

        if (regAppCheck) {
            application = this.selectRegApp(user.getMail(), user.getPassword());
        }

        return foundUser != null || application != null;
    }

    public boolean insertProfessor(RegistrationApplication professor) {
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("regInsProfessorData"),
                professor.getFullname(), professor.getMail(), professor.getPassword(),
                professor.getUniName(), professor.getContacts());

        return true;
    }

    public boolean insertStudent(User user) {
        if (this.studTypeId == -1) {
            try {
                this.studTypeId = this.getJdbcTemplate().queryForObject(
                        getDatabaseQuery().getProperty("regSelStudentTypeId"),
                        (rs, rowNum) -> rs.getInt("id"));
            } catch (EmptyResultDataAccessException ex) {
                this.studTypeId = -1;
                return false;
            }
        }
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("regInsUserData"),
                user.getFullname(), user.getMail(), user.getPassword(), this.studTypeId);

        return true;
    }
}
