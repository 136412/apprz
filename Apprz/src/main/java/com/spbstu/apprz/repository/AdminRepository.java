package com.spbstu.apprz.repository;

import com.spbstu.apprz.model.RegistrationApplication;
import com.spbstu.apprz.model.User;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class AdminRepository extends CommonRepository {
    private int profTypeId = -1;

    public List<User> selectOldUsers() {
        List<User> users;
        try {
            users = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("adminSelOldUserDataList"),
                    (rs, rowNum) ->
                            new User(rs.getInt("id"), rs.getString("fullname"), rs.getString("mail"), "",
                                    rs.getDate("creationTime"), rs.getString("name")));
        } catch (EmptyResultDataAccessException ex) {
            users = null;
        }

        return users;
    }


    public List<RegistrationApplication> selectProfessorRequests() {
        List<RegistrationApplication> regList;
        try {
            regList = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("adminSelRegistrationApplicationDataList"),
                    (rs, rowNum) ->
                            new RegistrationApplication(rs.getInt("id"), rs.getString("name"), rs.getString("mail"),
                                    rs.getString("universityName"),  rs.getString("contacts"),rs.getDate("appDate")));
        } catch (EmptyResultDataAccessException ex) {
            regList = null;
        }

        return regList;
    }


    public List<User> selectAllUsers() {
        List<User> users;
        try {
            users = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("adminSelUserDataList"),
                    (rs, rowNum) ->
                            new User(rs.getInt("id"), rs.getString("fullname"), rs.getString("mail"), "",
                                    rs.getDate("creationtime"), rs.getString("name")));
        } catch (EmptyResultDataAccessException ex) {
            users = null;
        }

        return users;
    }


    public boolean insertProfessor(RegistrationApplication application) {
        if (this.profTypeId == -1) {
            try {
                this.profTypeId = this.getJdbcTemplate().queryForObject(
                        getDatabaseQuery().getProperty("regSelProfessorTypeId"),
                        (rs, rowNum) -> rs.getInt("id"));
            } catch (EmptyResultDataAccessException ex) {
                this.profTypeId = -1;
                return false;
            }
        }

        // Insert in user table
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("regInsUserData"),
                application.getFullname(), application.getMail(), application.getPassword(), this.profTypeId);

        // Delete from reg app
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("adminDelProfessorApplicationByProfessorId"),
                application.getId());

        return true;
    }

    public RegistrationApplication selectProfById(int id) {
        RegistrationApplication application;
        try {
            application = this.getJdbcTemplate().queryForObject(
                    getDatabaseQuery().getProperty("adminSelProfessorDataByProfessorId"),
                    new Object[]{id},
                    (rs, rowNum) ->
                            new RegistrationApplication(rs.getInt("id"), rs.getString("name"), rs.getString("mail"),
                                    rs.getString("password"), rs.getString("universityName"), rs.getString("contacts"),
                                    rs.getDate("appDate")));
        } catch (EmptyResultDataAccessException ex) {
            application = null;
        }

        return application;
    }

    public boolean deleteUserById(int id) {
        // Delete from users
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("adminDelUserByUserId"), id);

        return true;
    }

}
