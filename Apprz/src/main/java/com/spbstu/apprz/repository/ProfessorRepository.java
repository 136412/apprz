package com.spbstu.apprz.repository;

import com.spbstu.apprz.model.*;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

@Repository
public class ProfessorRepository extends CommonRepository {


    public List<Test> selectProfessorTestListByProfessorId(int id) {
        List<Test> professorTestList;

        try {
            professorTestList = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("professorSelTestDataListByProfessorId"),
                    new Object[]{id},
                    (rs, rowNum) ->
                            new Test(rs.getInt("id"), rs.getString("name"), "",
                                    rs.getString("typeName"), rs.getFloat("mark")));
        } catch (EmptyResultDataAccessException ex) {
            professorTestList = null;
        }

        return professorTestList;
    }

    public List<Task> selectTaskList() {
        List<Task> professorTaskList;

        try {
            professorTaskList = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("professorSelAllTaskDataList"),
                    (rs, rowNum) ->
                            new Task(rs.getInt("id"), rs.getString("name"), rs.getString("themeName"),
                                    rs.getInt("difficulty"), rs.getString("fullname"), rs.getInt("userId")));
        } catch (EmptyResultDataAccessException ex) {
            professorTaskList = null;
        }

        return professorTaskList;
    }


    public Test selectTestDataByTestId(int id) {
        Test test;

        try {
            test = this.getJdbcTemplate().queryForObject(
                    getDatabaseQuery().getProperty("professorSelTestDataByTestId"),
                    new Object[]{id},
                    (rs, rowNum) -> new Test(rs.getString("name"), rs.getString("typeName"), rs.getInt("authorId")));
        } catch (EmptyResultDataAccessException ex) {
            test = null;
        }

        return test;
    }

    public Task selectTaskByTaskId(int id) {
        Task task;

        try {
            task = this.getJdbcTemplate().queryForObject(
                    getDatabaseQuery().getProperty("professorSelTaskByTaskId"),
                    new Object[]{id},
                    (rs, rowNum) ->
                            new Task(rs.getInt("id"), rs.getString("name"), rs.getString("themeName"), rs.getInt("difficulty"),
                                    rs.getString("condition"), rs.getString("answer"), rs.getString("fullname"), rs.getInt("authorId")));
        } catch (EmptyResultDataAccessException ex) {
            task = null;
        }

        return task;
    }


    public List<Task> selectTaskListInTestByTestId(int id) {
        List<Task> taskList;

        try {
            taskList = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("professorSelTaskDataListByTestId"),
                    new Object[]{id},
                    (rs, rowNum) ->
                            new Task(rs.getInt("id"), rs.getString("name"), rs.getString("themeName"),
                                    rs.getInt("difficulty"), rs.getString("fullname")));
        } catch (EmptyResultDataAccessException ex) {
            taskList = null;
        }

        return taskList;
    }

    public List<StudentResult> selectStudentResultsByProfessorId(int id) {
        List<StudentResult> studentResults;

        try {
            studentResults = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("professorSelTestResultDataListByAuthorId"),
                    new Object[]{id},
                    (rs, rowNum) ->
                            new StudentResult(rs.getInt("id"), rs.getString("fullname"), rs.getString("groupNames"), rs.getFloat("mark"),
                                    rs.getInt("testId"), rs.getInt("resultId"), rs.getTimestamp("timestamp")));
        } catch (EmptyResultDataAccessException ex) {
            studentResults = null;
        }

        return studentResults;
    }

    public List<TestStatistics> selectTestStatisticsByProfessorId(int id) {
        List<TestStatistics> statisticsList;

        try {
            statisticsList = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("professorSelTestStatListByAuthorId"),
                    new Object[] {id},
                    (rs, rowNum) ->
                            new TestStatistics(rs.getInt("id"), rs.getString("name"), rs.getString("typeName"),
                                    rs.getFloat("min"), rs.getFloat("max"), rs.getFloat("average"), rs.getInt("testId")
                            ));
        } catch (EmptyResultDataAccessException ex) {
            statisticsList = null;
        }

        return statisticsList;
    }


    public List<TestDetailedStatistics> selectTestDetailedStatisticsByTestId(int id) {
        List<TestDetailedStatistics> detailedStatistics;

        try {
            detailedStatistics = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("professorSelDetailedTestStatByTestId"),
                    new Object[] {id, id},
                    (rs, rowNum) ->
                            new TestDetailedStatistics(rs.getInt("userId"), rs.getString("fullname"), rs.getString("name"),
                                    rs.getString("groupNames"), rs.getInt("count"), rs.getFloat("firstMark"),
                                    rs.getFloat("lastMark")
                            ));
        } catch (EmptyResultDataAccessException ex) {
            detailedStatistics = null;
        }

        return detailedStatistics;
    }


    public List<TaskTheme> selectTaskThemeList() {
        List<TaskTheme> taskThemeList;

        try {
            taskThemeList = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("professorSelFullTaskThemeList"),
                    (rs, rowNum) ->
                            new TaskTheme(rs.getInt("id"), rs.getString("name")));
        } catch (EmptyResultDataAccessException ex) {
            taskThemeList = null;
        }

        return taskThemeList;
    }

    public List<StudentGroupName> selectStudentGroupNameList() {
        List<StudentGroupName> studentGroupNameList;

        try {
            studentGroupNameList = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("professorSelStudentGroupNameList"),
                    (rs, rowNum) ->
                            new StudentGroupName(rs.getInt("id"), rs.getString("name")));
        } catch (EmptyResultDataAccessException ex) {
            studentGroupNameList = null;
        }

        return studentGroupNameList;
    }

    public List<User> selectAllUserList() {
        List<User> userList;

        try {
            userList = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("professorSelUserDataList"),
                    (rs, rowNum) -> new User(rs.getInt("id"), rs.getString("fullname"), rs.getString("typeName"),
                            rs.getString("groupNames")));
        } catch (EmptyResultDataAccessException ex) {
            userList = null;
        }

        return userList;
    }

    public List<Integer> selectUserIdListByGroupId(int groupId) {
        List<Integer> userIdList;

        try {
            userIdList = this.getJdbcTemplate().query(
                    getDatabaseQuery().getProperty("professorSelStudentGroupIdListByStudentGroupId"),
                    new Object[] {groupId},
                    (rs, rowNum) -> rs.getInt("studentId"));
        } catch (EmptyResultDataAccessException ex) {
            userIdList = null;
        }

        return userIdList;
    }

    public boolean insertTestToTestList(int studentId, int testId) {
        Integer checkedStudentId;

        try {
            checkedStudentId = this.getJdbcTemplate().queryForObject(
                    getDatabaseQuery().getProperty("professorSelTestListData"),
                    new Object[]{studentId, testId},
                    (rs, rowNum) -> rs.getInt("studentId"));
        } catch (EmptyResultDataAccessException ex) {
            checkedStudentId = null;
        }

        if (checkedStudentId == null) {
            this.getJdbcTemplate().update(getDatabaseQuery().getProperty("professorInsTestData"),
                    studentId, testId);
        }
        return true;
    }

    public boolean checkTaskInTestByTaskId(int id) {
        Integer count;

        try {
            count = this.getJdbcTemplate().queryForObject(
                    getDatabaseQuery().getProperty("professorSelTaskTestIdListByTaskId"),
                    new Object[]{id},
                    (rs, rowNum) -> rs.getInt("count"));
        } catch (EmptyResultDataAccessException ex) {
            count = 0;
        }

        return count != 0;
    }

    // Only, if there is no test, whick has this task
    public boolean deleteTask(int id) {
        // resulterrors - del by taskResultId (got by taslId)
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("professorDelResultErrorsByTaskId"), id);

        // taskresult - del by taskId
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("professorDelTaskResultByTaskId"), id);

        // task
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("professorDelTaskByTaskId"), id);

        return true;
    }

    // Anytime, anywhere
    public boolean deleteTest(int id) {
        // teststatictics - del by testId
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("professorDelTestStatByTestId"), id);

        // tasklist - del by testId
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("professorDelTaskListByTestId"), id);

        // testresults - del by testId
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("professorDelTestResultsByTestId"), id);

        // testunmarked - del by testId
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("professorDelTestUnmarkedByTestId"), id);

        // testlist - del by testId
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("professorDelTestListByTestId"), id);

        // test - del
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("professorDelTestByTestId"), id);

        return true;
    }

    /* 'Create' modal queries */

    public boolean checkTaskByName(String name) {
        Task task;

        try {
            task = this.getJdbcTemplate().queryForObject(
                    getDatabaseQuery().getProperty("professorSelTaskDataListByTaskName"),
                    new Object[]{name},
                    (rs, rowNum) ->
                            new Task(rs.getInt("id"), rs.getString("name")));
        } catch (EmptyResultDataAccessException ex) {
            task = null;
        }

        return task != null;
    }


    public boolean insertTask(Task task) {
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("professorInsTaskData"),
                task.getName(), task.getThemeId(), task.getDifficulty(), task.getCondition(), "", task.getAnswer(), task.getAuthorId());

        return true;
    }

    public boolean checkTestByName(String name) {
        Test test;

        try {
            test = this.getJdbcTemplate().queryForObject(
                    getDatabaseQuery().getProperty("professorSelTestDataListByTestName"),
                    new Object[]{name},
                    (rs, rowNum) ->
                            new Test(rs.getInt("id"), rs.getString("name")));
        } catch (EmptyResultDataAccessException ex) {
            test = null;
        }

        return test != null;
    }

    public boolean insertTest(Test test, List<Integer> taskIdList) {
        GeneratedKeyHolder holder = new GeneratedKeyHolder();
        this.getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement statement = con.prepareStatement(
                        getDatabaseQuery().getProperty("professorInsControlTestData"), Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, test.getName());
                statement.setInt(2, test.getTypeId());
                statement.setTime(3, test.getTimeConsumed());
                statement.setInt(4, test.getAuthorId());
                return statement;
            }
        }, holder);

        int testId = holder.getKey().intValue();

        taskIdList.forEach((id) -> insertTaskToTest(id, testId));

        return true;
    }

    public boolean insertTaskToTest(int taskId, int testId) {
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("professorInsTaskListData"),
                taskId, testId);

        return true;
    }

    public boolean checkStudentGroupNameByName(String name) {
        StudentGroupName studentGroupName;

        try {
            studentGroupName = this.getJdbcTemplate().queryForObject(
                    getDatabaseQuery().getProperty("professorSelStudentGroupDataListByStudentGroupName"),
                    new Object[] {name},
                    (rs, rowNum) ->
                            new StudentGroupName(rs.getInt("id"), rs.getString("name")));
        } catch (EmptyResultDataAccessException ex) {
            studentGroupName = null;
        }

        return studentGroupName != null;
    }

    public boolean insertStudentGroup(String name, List<Integer> idList) {
        GeneratedKeyHolder holder = new GeneratedKeyHolder();
        this.getJdbcTemplate().update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement statement = con.prepareStatement(
                        getDatabaseQuery().getProperty("professorInsStudentGroupName"), Statement.RETURN_GENERATED_KEYS);
                statement.setString(1, name);
                return statement;
            }
        }, holder);

        int groupId = holder.getKey().intValue();

        idList.forEach((id) -> insertStudentToGroup(id, groupId));

        return true;
    }

    public boolean insertStudentToGroup(int studentId, int groupId) {
        this.getJdbcTemplate().update(getDatabaseQuery().getProperty("professorInsStudentGroup"),
                studentId, groupId);

        return true;
    }
}
