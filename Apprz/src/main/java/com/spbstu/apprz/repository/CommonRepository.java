package com.spbstu.apprz.repository;

import com.spbstu.apprz.model.RegistrationApplication;
import com.spbstu.apprz.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.Properties;

public class CommonRepository {
    private JdbcTemplate jdbcTemplate;
    private Properties databaseQuery;

    @Autowired
    public void setDatabaseQuery(Properties databaseQuery) {
        this.databaseQuery = databaseQuery;
    }

    public Properties getDatabaseQuery() {
        return databaseQuery;
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    public String getQuery(String propName){
        return databaseQuery.getProperty(propName);
    }

    public User selectUser(String mail, String password) {
        User user;
        try {
            user = this.getJdbcTemplate().queryForObject(
                    getDatabaseQuery().getProperty("regSelUserData"),
                    new Object[]{mail, password},
                    (rs, rowNum) ->
                            new User(rs.getInt("id"), rs.getString("fullname"), rs.getString("mail"), "", null, rs.getString("name")));
        } catch (EmptyResultDataAccessException ex) {
            user = null;
        }

        return user;
    }

    public RegistrationApplication selectRegApp(String mail, String password) {
        RegistrationApplication application;
        try {
            application = this.getJdbcTemplate().queryForObject(
                    getDatabaseQuery().getProperty("regSelRegApplicationData"),
                    new Object[]{mail, password},
                    (rs, rowNum) ->
                            new RegistrationApplication(rs.getInt("id"), rs.getString("name"), rs.getString("mail"),
                                    rs.getString("universityName"), rs.getString("contacts"), rs.getDate("appDate")));
        } catch (EmptyResultDataAccessException ex) {
            application = null;
        }

        return application;
    }
}