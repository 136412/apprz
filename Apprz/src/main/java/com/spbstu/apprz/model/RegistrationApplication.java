package com.spbstu.apprz.model;


import java.sql.Date;

public class RegistrationApplication extends User {
    private String uniName;
    private String contacts;
    private Date appDate;

    public RegistrationApplication() {
    }

    public RegistrationApplication(String fullname, String mail, String password, String uniName, String contacts) {
        super(fullname, mail, password);
        this.uniName = uniName;
        this.contacts = contacts;
    }

    public RegistrationApplication(int id, String fullname, String mail, String uniName, String contacts, Date appDate) {
        super(id, fullname, mail, "", null, "");
        this.uniName = uniName;
        this.contacts = contacts;
        this.appDate = appDate;
    }

    public RegistrationApplication(int id, String fullname, String mail, String password, String uniName, String contacts, Date appDate) {
        super(id, fullname, mail, password, null, "");
        this.uniName = uniName;
        this.contacts = contacts;
        this.appDate = appDate;
    }

    public String getUniName() {
        return uniName;
    }

    public void setUniName(String uniName) {
        this.uniName = uniName;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public Date getAppDate() {
        return appDate;
    }

    public void setAppDate(Date appDate) {
        this.appDate = appDate;
    }
}
