package com.spbstu.apprz.model;

import java.sql.Time;

public class Test {
    private int id;
    private String name;
    private String type; // Inter -
    private int typeId;  // - changable
    private float mark;
    private int numOfMarks;
    private Time timeConsumed;
    private int authorId;  // Inter -
    private String author; // - changeable

    public Test() {
    }

    public Test(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Test(String name, String type, int authorId) {
        this.name = name;
        this.type = type;
        this.authorId = authorId;
    }

    public Test(String name, int typeId, int authorId, Time timeConsumed) {
        this.name = name;
        this.typeId = typeId;
        this.authorId = authorId;
        this.timeConsumed = timeConsumed;
    }

    public Test(int id, String name, String author, String type) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.type = type;
    }

    public Test(int id, String name, String author, String type, Time timeConsumed) {
        this(id, name, author, type);
        this.timeConsumed = timeConsumed;
    }

    public Test(int id, String name, String author, String type, float mark) {
        this(id, name, author, type);
        this.mark = mark;
    }

    public Test(int id, String name, String author, String type, float mark, Time timeConsumed) {
        this(id, name, author, type);
        this.mark = mark;
        this.timeConsumed = timeConsumed;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public float getMark() {
        return mark;
    }

    public void setMark(float mark) {
        this.mark = mark;
    }

    public int getNumOfMarks() {
        return numOfMarks;
    }

    public void setNumOfMarks(int numOfMarks) {
        this.numOfMarks = numOfMarks;
    }

    public Time getTimeConsumed() {
        return timeConsumed;
    }

    public void setTimeConsumed(Time timeConsumed) {
        this.timeConsumed = timeConsumed;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
