package com.spbstu.apprz.model;


public class TaskResult {
    private int id;
    private int taskId;
    private int resultId;
    private String answer;


    public TaskResult(int id, int taskId, int resultId, String answer) {
        this.id = id;
        this.taskId = taskId;
        this.resultId = resultId;
        this.answer = answer;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public int getResultId() {
        return resultId;
    }

    public void setResultId(int resultId) {
        this.resultId = resultId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
