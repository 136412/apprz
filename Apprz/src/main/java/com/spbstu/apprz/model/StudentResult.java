package com.spbstu.apprz.model;

import java.sql.Timestamp;

public class StudentResult {
    private int id;
    private String name;
    private String groupName;
    private float mark;
    private int testId;
    private int resultId;
    private Timestamp testHeld;

    public StudentResult(int id, String name, String groupName, float mark, int testId, int resultId, Timestamp testHeld) {
        this.id = id;
        this.name = name;
        this.groupName = groupName;
        this.mark = mark;
        this.testId = testId;
        this.resultId = resultId;
        this.testHeld = testHeld;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public float getMark() {
        return mark;
    }

    public void setMark(float mark) {
        this.mark = mark;
    }

    public int getTestId() {
        return testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    public int getResultId() {
        return resultId;
    }

    public void setResultId(int resultId) {
        this.resultId = resultId;
    }

    public Timestamp getTestHeld() {
        return testHeld;
    }

    public void setTestHeld(Timestamp testHeld) {
        this.testHeld = testHeld;
    }
}
