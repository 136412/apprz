package com.spbstu.apprz.model;


public class Task {
    private int id;
    private String themeName; // Inter -
    private int themeId;      // - changeable
    private String condition;
    private int difficulty;
    private String solution; // Actually, part of 'answer' in DB. Consists of all but last part of 'answer'
    private String answer;   // Last part of 'answer'
    private String name;
    private int authorId;      // Both data fields are
    private String authorName; // interchangeable

    public Task() {
    }

    public Task(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Task(int id, String condition, String name) {
        this.id = id;
        this.condition = condition;
        this.name = name;
    }

    public Task(int id, String condition, String name, String answer, int authorId) {
        this.id = id;
        this.condition = condition;
        this.name = name;
        this.answer = answer;
        this.authorId = authorId;
    }

    public Task(int id, String name, String themeName, int difficulty, String authorName) {
        this.id = id;
        this.name = name;
        this.themeName = themeName;
        this.difficulty = difficulty;
        this.authorName = authorName;
    }

    public Task(String name, int themeId, String condition, int difficulty, String answer, int authorId) {
        this.name = name;
        this.themeId = themeId;
        this.condition = condition;
        this.difficulty = difficulty;
        this.answer = answer;
        this.authorId = authorId;
    }

    public Task(int id, String name, String themeName, int difficulty, String condition, String answer, String authorName,
                int authorId) {
        this.id = id;
        this.name = name;
        this.themeName = themeName;
        this.difficulty = difficulty;
        this.condition = condition;
        this.answer = answer;
        this.authorName = authorName;
        this.authorId = authorId;
    }

    public Task(int id, String name, String themeName, int difficulty, String authorName, int authorId) {
        this(id, name, themeName, difficulty, authorName);
        this.authorId = authorId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getThemeName() {
        return themeName;
    }

    public void setThemeName(String themeName) {
        this.themeName = themeName;
    }

    public int getThemeId() {
        return themeId;
    }

    public void setThemeId(int themeId) {
        this.themeId = themeId;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }
}
