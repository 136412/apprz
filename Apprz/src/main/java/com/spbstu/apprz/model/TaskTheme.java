package com.spbstu.apprz.model;

public class TaskTheme {
    private int id;
    private String name;
    private int solverId;

    public TaskTheme(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSolverId() {
        return solverId;
    }

    public void setSolverId(int solverId) {
        this.solverId = solverId;
    }
}
