package com.spbstu.apprz.model;

import java.sql.Date;

public class User {
    private int id;
    private String fullname;
    private String mail;
    private String password;
    private Date creationTime;
    private String type;
    private String group; // Has meaning for 'student' class users. Else, is '-'

    public User() {
    }

    public User(String fullname, String mail, String password) {
        this.fullname = fullname;
        this.mail = mail;
        this.password = password;
    }

    public User(int id, String fullname, String type, String group) {
        this.id = id;
        this.fullname = fullname;
        this.type = type;
        this.group = group;
    }

    public User(int id, String fullname, String mail, String password, Date creationTime, String type) {
        this.id = id;
        this.fullname = fullname;
        this.mail = mail;
        this.password = password;
        this.creationTime = creationTime ;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreationTime() {
        return creationTime ;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime  = creationTime ;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }
}
