package com.spbstu.apprz.model;

public class TestResult {
    private int id;
    private String name;
    private String type;
    private float mark;
    private String author;
    private int resultId;

    public TestResult() {
    }

    public TestResult(int id, String name, String type, float mark, String author, int resultId) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.mark = mark;
        this.author = author;
        this.resultId = resultId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public float getMark() {
        return mark;
    }

    public void setMark(float mark) {
        this.mark = mark;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getResultId() {
        return resultId;
    }

    public void setResultId(int resultId) {
        this.resultId = resultId;
    }
}
