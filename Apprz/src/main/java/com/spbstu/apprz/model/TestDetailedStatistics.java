package com.spbstu.apprz.model;

public class TestDetailedStatistics {
    private int id;
    private String fullname;
    private String name;
    private String groupName;
    private int count;
    private float firstMark;
    private float lastMark;

    public TestDetailedStatistics(int id, String fullname, String name, String groupName, int count, float firstMark, float lastMark) {
        this.id = id;
        this.fullname = fullname;
        this.name = name;
        this.groupName = groupName;
        this.count = count;
        this.firstMark = firstMark;
        this.lastMark = lastMark;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public float getFirstMark() {
        return firstMark;
    }

    public void setFirstMark(float firstMark) {
        this.firstMark = firstMark;
    }

    public float getLastMark() {
        return lastMark;
    }

    public void setLastMark(float lastMark) {
        this.lastMark = lastMark;
    }
}
