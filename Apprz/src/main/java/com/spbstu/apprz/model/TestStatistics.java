package com.spbstu.apprz.model;


public class TestStatistics {
    private int id;
    private float min;
    private float max;
    private float average;
    private int numOfAttempts;
    private int testId;
    private String name;     // Obtainable via
    private String typeName; // 'testId' field

    public TestStatistics() {
    }

    public TestStatistics(int id, float min, float max, float average, int testId, int numOfAttempts) {
        this.id = id;
        this.min = min;
        this.max = max;
        this.average = average;
        this.testId = testId;
        this.numOfAttempts = numOfAttempts;
    }

    public TestStatistics(int id, String name, String typeName, float min, float max, float average, int testId) {
        this.id = id;
        this.name = name;
        this.typeName = typeName;
        this.min = min;
        this.max = max;
        this.average = average;
        this.testId = testId;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getMin() {
        return min;
    }

    public void setMin(float min) {
        this.min = min;
    }

    public float getMax() {
        return max;
    }

    public void setMax(float max) {
        this.max = max;
    }

    public float getAverage() {
        return average;
    }

    public void setAverage(float average) {
        this.average = average;
    }

    public int getNumOfAttempts() {
        return numOfAttempts;
    }

    public void setNumOfAttempts(int numOfAttempts) {
        this.numOfAttempts = numOfAttempts;
    }

    public int getTestId() {
        return testId;
    }

    public void setTestId(int testId) {
        this.testId = testId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
