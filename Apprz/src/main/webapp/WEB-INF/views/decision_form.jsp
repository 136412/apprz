<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Welcome</title>

	<link rel="stylesheet" type="text/css" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/imagehover.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/style.css">

	<script src="resources/js/jquery.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/custom.js" type="text/javascript"></script>

	<style type="text/css">
		textarea {
          width:100%;
        }
		.form-control {
		   border: 1px solid #000000;
		}
		.btn-default {
			border: 1px solid #000000;
		}
		.login-box-msg {
			color: #000000;
		}
		.container {
			color: #000000;
		}
		.table {
			color: #000000;
		}
		.tab-content {
			color: #000000;
		}
	</style>
</head>

<body>

	<!--Navigation bar-->
	<nav class="navbar navbar-default navbar-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="navbar-brand">Ora<span>cle</span></div>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav navbar-right">
				<li class="btn-trial">
					<a href="/index" >Выйти из системы</a>
				</li>
			</ul>
		</div>
	</div>
	</nav>
	<!--/ Navigation bar-->

	<div class="container">
		Вы зашли в систему как: <label for="name"><c:out value="${student_name}"></c:out></label>
	</div>
	
    <c:set var = "test_size" scope = "session" value = "${fn:length(my_test)}"/>
	
	<div style="text-align: center" class="container-fluid">
	<div style="text-align: center" class="container-fluid">
		<div class="row">
			
			<div class="col-md-2">
				<p id="test_time" style="margin-top: 30px"></p>
				<script type="text/javascript">
					var testDist = <c:out value="${test_length}" />;
					if (testDist != 0) {
						var countDownDate = new Date().getTime();
						countDownDate = countDownDate + testDist;
						var x = setInterval(
								function() {

									// Get todays date and time
									var now = new Date().getTime();

									// Find the distance between now an the count down date
									var distance = countDownDate - now;

									// Time calculations for hours, minutes and seconds
									var hours = Math
											.floor((distance % (1000 * 60 * 60 * 24))
													/ (1000 * 60 * 60));
									var minutes = Math
											.floor((distance % (1000 * 60 * 60))
													/ (1000 * 60));
									var seconds = Math
											.floor((distance % (1000 * 60)) / 1000);

									document.getElementById("test_time").innerHTML = hours
											+ "h "
											+ minutes
											+ "m "
											+ seconds + "s ";

									// If the count down is finished, write some text 
									if (distance < 0) {
										clearInterval(x);
										document.getElementById("test_time").innerHTML = "EXPIRED";
										$('#out_of_time_test').modal('show');
									}
								}, 1000);
					}
					
					function submitTest() {
						$('form#testform').submit();
					}

					function eraseText(area_id) {
						document.getElementById("area".concat(area_id.toString())).value = "";
					}

					function finishTest() {
						var textAreaList = $('textarea'),
							answerList = $('input'),
							testData = "";

						for (var i = 0; i < textAreaList.length; i += 1) {
							testData += textAreaList[i].value + "\r\n" + answerList[i].value;
							if (i != textAreaList.length - 1) {
								testData += "@";
							}
						}
						$('input[name=testData]').val(testData);
						submitTest();
					}

					function toTestList() {
						submitTest();
					}
				</script>
			</div>
			
		</div>
	</div>
	
	<form action="/decision_form" id="testform" method="POST">
		<div class="tab-content clearfix">
			<c:forEach var="task" items="${my_test}" varStatus="loopS">
			
				<c:choose>
					<c:when test="${loopS.index == 0}">
						<div class="tab-pane active" id="<c:out value="${loopS.index}"/>a">
					</c:when>
					<c:otherwise>
						<div class="tab-pane" id="<c:out value="${loopS.index}"/>a">
					</c:otherwise>
				</c:choose>
					
					<div style="text-align: center" class="container-fluid">
						<div class="column">
							<div class="col-md-4">
								<h1>
									Задача <label for="current_task"><c:out value="${loopS.index + 1}"/></label> из <label
										for="all_task"><c:out value="${test_size}"></c:out></label>
								</h1>
							</div>
							<br/>
							<div class="col-md">
								<button type="button" class="btn btn-green" data-toggle="tab"
										href="#<c:out value="${loopS.index - 1}"/>a">&laquo;Предыдущая</button>
								<button type="button" class="btn btn-green" data-toggle="tab"
										href="#<c:out value="${loopS.index + 1}"/>a">Следующая&raquo;</button>

							</div>
						</div>
					</div>
					<hr/>
					<label for="description_task">
						<c:out value="${task.condition}"></c:out>
					</label>

					<div class="input-group" style="width: 100%; padding-bottom:30px; padding-top:10px;">
						<p>
							<textarea id="area<c:out value="${loopS.index}"/>" class="form-control" style="height:300px;" name="text"></textarea>
						</p>
					</div>

					<div class="row">
						<div class="column" style="float: left; margin-left: 15px;">
							<label for="comment">Ответ</label>
						</div>
						<div class="input-group row" style="width: 80%; float: left; margin-left: 20px; margin-right: 20px;">
							<input id="input<c:out value="${loopS.index}"/>" type="text" class="form-control" name="answer">
						</div>
						<div style="float: left; padding-left: 30px; margin-top: -5px;">
							<button type="button" class="btn btn-green" onclick="javascript:eraseText(<c:out value="${loopS.index}"/>);">Очистить</button>
						</div>
					</div>
				</div>
			</c:forEach>
		</div>
		<hr/>
		<div class="col-md">

			<button type="button" onclick="toTestList();" class="btn btn-green">К списку тестов</button>
			<input type="hidden" id="testData" name="testData" value="">
			<button type="submit" onclick="finishTest();" class="btn btn-green">Завершить тест</button>
	
		</div>
	</form>

	<div class="modal fade" id="out_of_time_test" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Внимание!</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Время выполнения теста закончилось. Все введенные решения были
						сохранены. В разделе Результаты тестов появится количество баллов, полученных Вами за этот тест </p>
						<div class="form-group">
							<form action="/decision_form" id="quitForm" method="POST">
								<div class="form-group has-feedback"></div>
								<div class="form-group has-feedback"></div>
								<div class="row">
									<div class="col-xs-12">
										<button type="button" onclick="finishTest();" class="btn btn-green btn-block btn-flat">OK</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</body>
</html>