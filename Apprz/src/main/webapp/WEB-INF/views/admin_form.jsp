<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Welcome</title>

	<link rel="stylesheet" type="text/css" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/imagehover.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/style.css">

    <script src="resources/js/jquery.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/custom.js" type="text/javascript"></script>
	<style type="text/css">
		
		.form-control {
		   border: 1px solid #000000;
		}
		.btn-default {
			border: 1px solid #000000;
		}
		.login-box-msg {
			color: #000000;
		}
		.container {
			color: #000000;
		}
		.table {
			color: #000000;
		}
		.tab-content {
			color: #000000;
		}
	</style>
</head>

<body>
	<script type="text/javascript">
		function showAll() {
			var $rowsNo = $('#regUsers tbody tr').filter(function () {
					return true
				}).show();
		}

		function showStud() {
			showAll();
        	var $rowsNo = $('#regUsers tbody tr').filter(function () {
					return $.trim($(this).find('td').eq(3).text()) !== "студент"
				}).hide();
        }

        function showProff() {
        	showAll();
        	var $rowsNo = $('#regUsers tbody tr').filter(function () {
					return $.trim($(this).find('td').eq(3).text()) !== "преподаватель"
				}).hide();
        }

		function changeDelUsers() {
			var ids = $('.admin-delete-user input:checked').map(function () {
                return this.value;
            }).get();
			console.log(ids);

			if (ids.length == 0) {
				$("#error_del_choose").modal();
			} else {
				$('input[name=deletedUserValue]').val(ids);
				$("#access-deleting").modal();
			}
		}
		console.log("${requestScope['wasAdded']}");
		console.log("${requestScope['wasDeleted']}");

        <c:if test="${requestScope['wasAdded'] == 'true'}">
        	$(window).on('load',function(){
				$('#confirm-registration').modal('show');
			});
        </c:if>

		<c:if test="${requestScope['wasDeleted']  == 'true'}">
			$(window).on('load',function(){
				$('#confirm-deleting').modal('show');
			});
        </c:if>

	</script>

	<!--Navigation bar-->
	<nav class="navbar navbar-default navbar-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#myNavbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<div class="navbar-brand">Ora<span>cle</span></div>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="/index">Выйти</a></li>
			</ul>
		</div>
	</div>
	</nav>

	<div class="container">
	    
		<h3>Вы зашли в систему как: <c:out value="${admin_name}"></c:out></h3>
	</div>
	<div id="exTab1" class="container">
		<ul class="nav nav-pills">
			<c:choose>
				<c:when test="${requestScope['tabID'] == null || requestScope['tabID'] == '1a'}">
					<li class="active"><a href="#1a" data-toggle="tab">Зарегистрированные пользователи</a></li>
					<li><a href="#2a" data-toggle="tab">Заявки на регистрацию</a></li>
					<li><a href="#3a" data-toggle="tab">Удалить пользователей</a></li>
				</c:when>

				<c:when test="requestScope['tabID'] == '2a'}">
					<li><a href="#1a" data-toggle="tab">Зарегистрированные пользователи</a></li>
					<li class="active"><a href="#2a" data-toggle="tab">Заявки на регистрацию</a></li>
					<li><a href="#3a" data-toggle="tab">Удалить пользователей</a></li>
				</c:when>

				<c:when test="requestScope['tabID'] == '3a'}">
					<li><a href="#1a" data-toggle="tab">Зарегистрированные пользователи</a></li>
					<li><a href="#2a" data-toggle="tab">Заявки на регистрацию</a></li>
					<li class="active"><a href="#3a" data-toggle="tab">Удалить пользователей</a></li>
				</c:when>

				<c:otherwise>
					<li><a href="#1a" data-toggle="tab">Зарегистрированные пользователи</a></li>
					<li><a href="#2a" data-toggle="tab">Заявки на регистрацию</a></li>
					<li><a href="#3a" data-toggle="tab">Удалить пользователей</a></li>
				</c:otherwise>
			</c:choose>
		</ul>
		<hr/>
		<div class="tab-content clearfix">
			<c:choose>
				<c:when test="${requestScope['tabID'] == null || requestScope['tabID'] == '1a'}">
				   <div class="tab-pane active" id="1a">
				</c:when>
				<c:otherwise>
					<div class="tab-pane" id="1a">
				</c:otherwise>
			</c:choose>

				<div class="modal-body" style="padding: 0px;">
					<div class="login-box-body">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-primary active" onclick="showAll();">
								<input type="radio" name="options" id="option1" autocomplete="off" checked> Все пользователи
							</label>
							<label class="btn btn-primary" onclick="showProff();">
								<input type="radio" name="options" id="option2" autocomplete="off"> Только преподаватели
							</label>
							<label class="btn btn-primary" onclick="showStud();">
								<input type="radio" name="options" id="option3" autocomplete="off"> Только студенты
							</label>
						</div>
					</div>
				</div>
				<div>
					<table id="regUsers" class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>ФИО</th>
								<th>e-mail</th>
								<th>тип пользователя</th>
								<th>дата регистрации</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="user" items="${users}">
								<tr class="${user.type}">
									<td>${user.id}</td>
									<td>${user.fullname}</td>
									<td>${user.mail}</td>
									<td>${user.type}</td>
									<td>${user.creationTime}</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>

			<c:choose>
				<c:when test="${requestScope['tabID'] != null && requestScope['tabID'] == '2a'}">
				   <div class="tab-pane active" id="2a">
				</c:when>
				<c:otherwise>
					<div class="tab-pane" id="2a">
				</c:otherwise>
			</c:choose>
			
				<div class="admin-proffessor-reg">
					<table class="table">
						<thead>
							<tr>
								<th>ФИО</th>
								<th>e-mail</th>
								<th>учебное заведение</th>
								<th>контакты</th>
								<th>дата подачи заявки</th>
								<th>зарегистрировать?</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="user" items="${usersReg}">
								<tr>
									<td>${user.fullname}</td>
									<td>${user.mail}</td>
									<td>${user.uniName}</td>
									<td>${user.contacts}</td>
									<td>${user.appDate}</td>
									<td><form action="/admin_form" method="POST">
											<input type="hidden" name="proffRegId" id="proffRegId" value="<c:out value="${user.id}"/>">
											<input type="hidden" name="tabID" value="2a" />
											<button type="submit" class="btn btn-default">Зарегистрировать!</button>
										</form>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>

			<c:choose>
				<c:when test="${requestScope['tabID'] != null && requestScope['tabID'] == '3a'}">
				   <div class="tab-pane active" id="3a">
				</c:when>
				<c:otherwise>
					<div class="tab-pane" id="3a">
				</c:otherwise>
			</c:choose>

				<h5>Внимание! В этой таблице представлены только пользователи,
					зарегистрированные в системе более 6 лет.</h5>
				<h5>Других пользователей нельзя удалять из системы.</h5>
				<div class="admin-delete-user">

				<jsp:useBean id="now" class="java.util.Date" scope="request"/>
				<fmt:parseNumber value="${now.time / (1000*60*60*24)}" integerOnly="true" var="nowDays" scope="request"/>

				<form action="/admin_form" method="POST">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>id</th>
								<th>ФИО</th>
								<th>e-mail</th>
								<th>тип пользователя</th>
								<th>дата регистрации</th>
								<th>всего в системе</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="userDel" items="${usersDel}">
								<fmt:parseNumber value="${userDel.creationTime.time / (1000*60*60*24)}" integerOnly="true" var="otherDays" scope="page"/>
								<c:set value="${nowDays - otherDays}" var="dateDiff"/>
								<tr>
									<td><label class="checkbox-inline"><input
										type="checkbox" name="deletedUser" value="<c:out value="${userDel.id}"/>" ></label></td>
									<td>${userDel.id}</td>
									<td>${userDel.fullname}</td>
									<td>${userDel.mail}</td>
									<td>${userDel.type}</td>
									<td>${userDel.creationTime}</td>
									<td>${dateDiff} дней</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<button type="button" onclick="changeDelUsers();" class="btn btn-default">Удалить выделенных пользователей!</button>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="confirm-registration" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Поздравляем!</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Новый преподаватель был успешно
							зарегистрирован в системе! Пожалуйста, не забудьте уведомить
							пользователя о доступности его аккаунта.</p>
						<div class="form-group">
							<div class="form-group has-feedback"></div>
							<div class="form-group has-feedback"></div>
							<div class="row">
								<div class="col-xs-12">
									<button type="button" class="btn btn-green btn-block btn-flat" data-dismiss="modal">OK</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="confirm-deleting" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Поздравляем!</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Выбранные пользователи были удалены
							из системы.</p>
						<div class="form-group">
							<div class="form-group has-feedback"></div>
							<div class="form-group has-feedback"></div>
							<div class="row">
								<div class="col-xs-12">
									<button type="button" class="btn btn-green btn-block btn-flat" data-dismiss="modal">OK</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="access-deleting" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Внимание!</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Выбранные пользователи будут удалены
							из системы. Вы уверены, что хотите их удалить?</p>
						<div class="form-group">
							<form action="/admin_form" method="POST">
								<div class="form-group has-feedback"></div>
								<div class="form-group has-feedback"></div>
								<input type="hidden" name="tabID" value="3a" />
								<input type="hidden" name="deletedUserValue" id="deletedUserValue" value="0"/>

								<div class="row">
									<div class="col-xs-6 previous">
										<button type="submit" class="btn btn-green btn-block btn-flat">Да
										</button>
									</div>
									<div class="col-xs-6 next">
										<button type="button" class="btn btn-green btn-block btn-flat" data-dismiss="modal">Отмена
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<!--Error modal box-->
	<div class="modal fade" id="error_del_choose" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content -->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Ошибка ввода данных</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Выберите хотя бы одного пользователя для удаления</p>
						<div class="form-group">
							<div class="form-group has-feedback">
							</div>
							<div class="form-group has-feedback">
							</div>
							<div class="row">
								<div class="col-xs-12">
									<button type="button" class="btn btn-green btn-block btn-flat" data-dismiss="modal">OK</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--Error modal box-->

</body>
</html>