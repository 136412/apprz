<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Регистрация</title>

	<link rel="stylesheet" type="text/css" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/imagehover.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/style.css">

    <script src="resources/js/jquery.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/custom.js" type="text/javascript"></script>
	
	<style type="text/css">
		.form-control {
		   border: 1px solid #000000;
		}
		.btn-default {
			border: 1px solid #000000;
		}
		.login-box-msg {
			color: #000000;
		}
		.container {
			color: #000000;
		}
		.table {
			color: #000000;
		}
		.tab-content {
			color: #000000;
		}
	</style>
</head>

<body>
	<script type="text/javascript">
		function checkIfStudentIsFilled() {
			if (!$('input[name=loginidSt]').val() || !$('input[name=emailSt]').val() || !$('input[name=loginpswSt]').val()) {
				$('#error_reg').modal('show');
			} else {
				$('form#regFormSt').submit();
			}
		}

		function checkIfProfessorIsFilled() {
			if (!$('input[name=loginidPr]').val() || !$('input[name=emailPr]').val() || !$('input[name=loginpswPr]').val() ||
				!$('input[name=universityPr]').val() || !$('input[name=contactsPr]').val()) {
				$('#error_reg').modal('show');
			} else {
				$('form#regFormPr').submit();
			}
		}

		<c:choose>
			<c:when test="${requestScope['userSt'] == 'exist' || requestScope['userPr'] == 'exist'}">
				$(window).on('load',function(){
					$('#error_reg_2').modal('show');
				});
			</c:when>

			<c:when test="${requestScope['userSt'] == 'created'}">
				$(window).on('load',function(){
					$('#confirm_reg_student').modal('show');
				});
			</c:when>

			<c:when test="${requestScope['userPr'] == 'created'}">
				$(window).on('load',function(){
					$('#confirm_reg_professor').modal('show');
				});
			</c:when>
		</c:choose>
	</script>

	<!--Navigation bar-->
	<nav class="navbar navbar-default navbar-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="navbar-brand">Ora<span>cle</span></div>
		</div>
	</div>
	</nav>
	<!--/ Navigation bar-->

	<div id="exTab1" class="container">
		<ul class="nav nav-pills">
			<%
			if (request.getParameter("tabID") == null ||  request.getParameter("tabID").equals("1a")) {
				%>
				<li class="active"><a href="#1a" data-toggle="tab">Студент</a></li>
			<% } else {%>
				<li><a href="#1a" data-toggle="tab">Студент</a></li>
			<%}
			%>
			<%
			if (request.getParameter("tabID") != null && request.getParameter("tabID").equals("2a")) {
				%>
				<li class="active"><a href="#2a" data-toggle="tab">Преподаватель</a></li>
			<% } else {%>
				<li><a href="#2a" data-toggle="tab">Преподаватель</a></li>
			<%}
			%>
		</ul>

		<div class="container">
			<h3>Регистрация</h3>
		</div>
		<div class="tab-content clearfix">
			<%
			if (request.getParameter("tabID") == null ||  request.getParameter("tabID").equals("1a")) {
				%>
				<div class="tab-pane active" id="1a">
			<% } else {%>
				<div class="tab-pane" id="1a">
			<%}
			%>
				<div class="">
					<div class="modal-body padtrbl">
						<div class="login-box-body">
							<div class="form-group">
								<form action="/registration_form" id="regFormSt" method="POST">
									<input type="hidden" name="tabID" value="1a" />
									<div class="form-group has-feedback">
										<input class="form-control" placeholder="ФИО" name="loginidSt"
											type="text" autocomplete="off" /> <span
											style="display: none; font-weight: bold; position: absolute; color: red; position: absolute; padding: 4px; font-size: 11px; background-color: rgba(128, 128, 128, 0.26); z-index: 17; right: 27px; top: 5px;"
											id="span_loginid"></span>
										<!---Alredy exists  ! -->

									</div>
									<div class="form-group has-feedback">
										<input class="form-control" placeholder="E-mail"
											name="emailSt" type="text" autocomplete="off" /> <span
											style="display: none; font-weight: bold; position: absolute; color: red; position: absolute; padding: 4px; font-size: 11px; background-color: rgba(128, 128, 128, 0.26); z-index: 17; right: 27px; top: 5px;"
											id="span_loginid"></span>
										<!---Alredy exists  ! -->
									</div>

									<div class="form-group has-feedback">
										<input class="form-control" placeholder="Пароль"
											name="loginpswSt" type="password" autocomplete="off" /> <span
											style="display: none; font-weight: bold; position: absolute; color: grey; position: absolute; padding: 4px; font-size: 11px; background-color: rgba(128, 128, 128, 0.26); z-index: 17; right: 27px; top: 5px;"
											id="span_loginpsw"></span>
										<!---Alredy exists  ! -->

									</div>

									<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
										<div class="btn-group mr-2" role="group">
											<button type="button" onclick="checkIfStudentIsFilled();" class="btn btn-default">Регистрация</button>
										</div>
										<div class="btn-group mr-2" role="group">
											<a href="/index" class="btn btn-default">Отмена</a>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>

			</div>
			<%
			if (request.getParameter("tabID") != null && request.getParameter("tabID").equals("2a")) {
			%>
				<div class="tab-pane active" id="2a">
			<% } else {%>
				<div class="tab-pane" id="2a">
			<%}
			%>
				<div class="">
					<div class="modal-body padtrbl">
						<div class="login-box-body">
							<div class="form-group">
								<form action="/registration_form" id="regFormPr" method="POST">
									<input type="hidden" name="tabID" value="2a" />
									<div class="form-group has-feedback">
										<input class="form-control" placeholder="ФИО" name="loginidPr"
											type="text" autocomplete="off" /> <span
											style="display: none; font-weight: bold; position: absolute; color: red; position: absolute; padding: 4px; font-size: 11px; background-color: rgba(128, 128, 128, 0.26); z-index: 17; right: 27px; top: 5px;"
											id="span_loginid"></span>
									</div>
									<div class="form-group has-feedback">
										<input class="form-control" placeholder="E-mail"
											name="emailPr" type="text" autocomplete="off" /> <span
											style="display: none; font-weight: bold; position: absolute; color: red; position: absolute; padding: 4px; font-size: 11px; background-color: rgba(128, 128, 128, 0.26); z-index: 17; right: 27px; top: 5px;"
											id="span_loginid"></span>
									</div>

									<div class="form-group has-feedback">
										<input class="form-control" placeholder="Пароль"
											name="loginpswPr" type="password" autocomplete="off" /> <span
											style="display: none; font-weight: bold; position: absolute; color: grey; position: absolute; padding: 4px; font-size: 11px; background-color: rgba(128, 128, 128, 0.26); z-index: 17; right: 27px; top: 5px;"
											id="span_loginpsw"></span>
									</div>

									<div class="form-group has-feedback">
										<input class="form-control" placeholder="Учебное заведение"
											name="universityPr" type="text" autocomplete="off" /> <span
											style="display: none; font-weight: bold; position: absolute; color: red; position: absolute; padding: 4px; font-size: 11px; background-color: rgba(128, 128, 128, 0.26); z-index: 17; right: 27px; top: 5px;"
											id="span_loginid"></span>
									</div>
									<div class="form-group has-feedback">
										<input class="form-control"
											placeholder="Контакты учебного заведения" name="contactsPr"
											type="text" autocomplete="off" /> <span
											style="display: none; font-weight: bold; position: absolute; color: red; position: absolute; padding: 4px; font-size: 11px; background-color: rgba(128, 128, 128, 0.26); z-index: 17; right: 27px; top: 5px;"
											id="span_loginid"></span>
									</div>

									<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
										<div class="btn-group mr-2" role="group">
											<button type="button" onclick="checkIfProfessorIsFilled();" class="btn btn-default">Регистрация</button>
										</div>
										<div class="btn-group mr-2" role="group">
											<a href="/index" class="btn btn-default">Отмена</a>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



	<!--Modal box -->
	<div class="modal fade" id="error_reg" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Ошибка
						регистрации</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Заполнены не все поля</p>
						<div class="form-group">
							<div class="form-group has-feedback"></div>
							<div class="form-group has-feedback"></div>
							<div class="row">
								<div class="col-xs-12">
									<button type="submit" class="btn btn-green btn-block btn-flat" data-dismiss="modal">OK</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/ Modal box -->

	<!--Modal box -->
	<div class="modal fade" id="error_reg_2" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Ошибка
						регистрации</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Такой пользователь уже существует</p>
						<div class="form-group">
							<div class="form-group has-feedback"></div>
							<div class="form-group has-feedback"></div>
							<div class="row">
								<div class="col-xs-12">
									<button type="submit" class="btn btn-green btn-block btn-flat" data-dismiss="modal">OK</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/ Modal box -->

	<div class="modal fade" id="confirm_reg_student" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Поздравляем</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Вы успешно зарегистрированы в системе Оракул в качестве студента!</p>
						<div class="form-group">
							<div class="form-group has-feedback"></div>
							<div class="form-group has-feedback"></div>
							<div class="row">
								<div class="col-xs-12">
									<button type="button" class="btn btn-green btn-block btn-flat" onclick="location.href='/index'"
										data-dismiss="modal">OK</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="confirm_reg_professor" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Внимание</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Вы подали заявку на регистрацию в системе Оракул в качестве преподавателя! В случае успешной регистрации, Вы будете уведомлены об этом с помощью указанной Вами почты.</p>
						<div class="form-group">
							<div class="form-group has-feedback"></div>
							<div class="form-group has-feedback"></div>
							<div class="row">
								<div class="col-xs-12">
									<button type="submit" class="btn btn-green btn-block btn-flat" onclick="location.href='/index'"
										data-dismiss="modal">OK</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</div>
</body>
</html>