<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Oracle</title>

	<link rel="stylesheet" type="text/css" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/imagehover.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/style.css">

	<script src="resources/js/jquery.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/custom.js" type="text/javascript"></script>
	<style type="text/css">
		.form-control {
		   border: 1px solid #000000;
		}
		.btn-default {
			border: 1px solid #000000;
		}
		.login-box-msg {
			color: #000000;
		}
		.container {
			color: #000000;
		}
		.table {
			color: #000000;
		}
		.tab-content {
			color: #000000;
		}
	</style>
</head>

<body>
    <script type="text/javascript">
        function prepareMarkData(name) {
            var formId = "markForm" + name,
                markId = "mark" + name,
                selectId = "test_rating" + name;
            $('input#' + markId).val($('select#' + selectId).find("option:selected").attr("value"));
            $('form#' + formId).submit();
        }

    </script>

	<!--Navigation bar-->
	<nav class="navbar navbar-default navbar-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="navbar-brand">Ora<span>cle</span></div>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav navbar-right">
				<li class="btn-trial">
				    <a href="/index">Выйти из системы</a>
				 </li>
			</ul>
		</div>
	</div>
	</nav>
	<!--/ Navigation bar-->


	<div class="container">
		Вы зашли в систему как: <label for="name"><c:out value="${student_name}"></c:out></label>
	</div>
	<div id="exTab1" class="container">
		<ul class="nav nav-pills">
            <c:choose>
                <c:when test="${requestScope['tabID'] == null || requestScope['tabID'] == '1a'}">
                    <li class="active"><a href="#1a" data-toggle="tab">Назначенные тесты</a></li>
                    <li><a href="#2a" data-toggle="tab">Доступные тесты</a></li>
                    <li><a href="#3a" data-toggle="tab">Результаты тестов</a></li>
                    <li><a href="#4a" data-toggle="tab">Оценить тесты</a></li>
                </c:when>
                <c:when test="${requestScope['tabID'] == '2a'}">
                    <li><a href="#1a" data-toggle="tab">Назначенные тесты</a></li>
                    <li class="active"><a href="#2a" data-toggle="tab">Доступные тесты</a></li>
                    <li><a href="#3a" data-toggle="tab">Результаты тестов</a></li>
                    <li><a href="#4a" data-toggle="tab">Оценить тесты</a></li>
                </c:when>
                <c:when test="${requestScope['tabID'] == '3a'}">
                    <li><a href="#1a" data-toggle="tab">Назначенные тесты</a></li>
                    <li><a href="#2a" data-toggle="tab">Доступные тесты</a></li>
                    <li class="active"><a href="#3a" data-toggle="tab">Результаты тестов</a></li>
                    <li><a href="#4a" data-toggle="tab">Оценить тесты</a></li>
                </c:when>
                <c:when test="${requestScope['tabID'] == '4a'}">
                    <li><a href="#1a" data-toggle="tab">Назначенные тесты</a></li>
                    <li><a href="#2a" data-toggle="tab">Доступные тесты</a></li>
                    <li><a href="#3a" data-toggle="tab">Результаты тестов</a></li>
                    <li class="active"><a href="#4a" data-toggle="tab">Оценить тесты</a></li>
                </c:when>
                <c:otherwise>
                    <li><a href="#1a" data-toggle="tab">Назначенные тесты</a></li>
                    <li><a href="#2a" data-toggle="tab">Доступные тесты</a></li>
                    <li><a href="#3a" data-toggle="tab">Результаты тестов</a></li>
                    <li><a href="#4a" data-toggle="tab">Оценить тесты</a></li>
                </c:otherwise>
            </c:choose>
		</ul>
        <hr/>
		<div class="tab-content clearfix">
            <c:choose>
                <c:when test="${requestScope['tabID'] == null || requestScope['tabID'] == '1a'}">
                   <div class="tab-pane active" id="1a">
                </c:when>
                <c:otherwise>
                    <div class="tab-pane" id="1a">
                </c:otherwise>
            </c:choose>

				<div class="form-group">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Название</th>
								<th>Автор</th>
								<th>Тип теста</th>
								<th>Время выполения теста (в минутах)</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="test" items="${assignedTests}">
								<fmt:parseNumber value="${test.timeConsumed.time / (1000*60)}" integerOnly="true" var="minutesConsumed" scope="page"/>
								<tr>
									<td>${test.id}</td>
									<td>${test.name}</td>
									<td>${test.author}</td>
									<td>${test.type}</td>
									<td><c:if test="${minutesConsumed == 0}"> - </c:if>
										<c:if test="${minutesConsumed != 0}"> ${minutesConsumed} </c:if></td>
									<td>
                                        <form action="/student_form" method="POST">
                                            <input type="hidden" name="beginTest" value="true">
                                            <input type="hidden" name="timeConsumed" value="<c:out value="${minutesConsumed}" /> ">
                                            <input type="hidden" name="tabID" value="1a">
                                            <input type="hidden" name="testID" value="<c:out value="${test.id}" /> ">
                                            <button type="submit" class="btn btn-green">Начать</button>
                                        </form>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>

			<c:choose>
                <c:when test="${requestScope['tabID'] != null && requestScope['tabID'] == '2a'}">
                   <div class="tab-pane active" id="2a">
                </c:when>
                <c:otherwise>
                    <div class="tab-pane" id="2a">
                </c:otherwise>
            </c:choose>

				<div class="form-group">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Название</th>
								<th>Автор</th>
								<th>Тип теста</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="test" items="${availableTests}">
								<tr style="height:55px;">
									<td>${test.id}</td>
									<td>${test.name}</td>
									<td>${test.author}</td>
									<td>${test.type}</td>
                                    <td>
                                        <form action="/student_form" method="POST"
                                            style="<c:if test="${test.type == 'контрольная работа'}"> display: none; </c:if>">
                                            <input type="hidden" name="setMyTest" value="true">
                                            <input type="hidden" name="tabID" value="2a">
                                            <input type="hidden" name="testID" value="<c:out value="${test.id}" /> ">
                                            <button type="submit" class="btn btn-green">Назначить на себя тест</button>
                                        </form>
                                    </td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>

            <c:choose>
                <c:when test="${requestScope['tabID'] != null && requestScope['tabID'] == '3a'}">
                   <div class="tab-pane active" id="3a">
                </c:when>
                <c:otherwise>
                    <div class="tab-pane" id="3a">
                </c:otherwise>
            </c:choose>

				<div class="form-group">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Название</th>
								<th>Автор</th>
								<th>Тип теста</th>
								<th>Результаты</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="test" items="${resultsOfTheTests}">
								<tr>
									<td>${test.id}</td>
									<td>${test.name}</td>
									<td>${test.author}</td>
									<td>${test.type}</td>
									<td>${test.mark}</td>
									<td>
										<form action="/student_form" method="POST">
										    <input type="hidden" name="seeSolution" value="true">
										    <input type="hidden" name="tabID" value="3a">
											<input type="hidden" name="resultId" value="<c:out value="${test.resultId}" /> ">
											<button type="submit" class="btn btn-green">Просмотреть решение</button>
										</form>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>

            <c:choose>
                <c:when test="${requestScope['tabID'] != null && requestScope['tabID'] == '4a'}">
                   <div class="tab-pane active" id="4a">
                </c:when>
                <c:otherwise>
                    <div class="tab-pane" id="4a">
                </c:otherwise>
            </c:choose>

				<div class="form-group">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Название</th>
								<th>Автор</th>
								<th>Тип теста</th>
								<th>Оценка</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="test" items="${evaluateTests}">
								<tr>
									<td>${test.id}</td>
									<td>${test.name}</td>
									<td>${test.author}</td>
									<td>${test.type}</td>
									<form action="/student_form" id="markForm<c:out value="${test.id}" />" method="POST">
										<td><select class="form-control" id="test_rating<c:out value="${test.id}" />">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
										</select></td>
										<td>
                                            <input type="hidden" name="setMark" value="true">
                                            <input type="hidden" name="tabID" value="4a">
                                            <input type="hidden" id="mark<c:out value="${test.id}" />" name="mark" value="1">
                                            <input type="hidden" name="testID" value="<c:out value="${test.id}" />">
                                            <button type="button" class="btn btn-green btn-space" onclick="prepareMarkData('<c:out value="${test.id}" />');">Оценить</button>
										</td>
									</form>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
    </div>
</body>
</html>