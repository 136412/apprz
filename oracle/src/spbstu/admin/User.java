package spbstu.admin;

public class User implements Cloneable {
	private String fullName;
	private String eMail;
	private String type;
	private String date;
	private String id;

	public User() {		
	}
	
	public User(String id, String fullName, String eMail, String type, String date) {
		this.id = id;
		this.fullName = fullName;
		this.eMail = eMail;
		this.type = type;
		this.date = date;
	}

	public String getfullName() {
		return this.fullName;
	}

	public void setfullName(String fullName) {
		this.fullName = fullName;
	}

	public String geteMail() {
		return this.eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String gettype() {
		return this.type;
	}

	public void settype(String type) {
		this.type = type;
	}

	public String getdate() {
		return this.date;
	}

	public void setdate(String date) {
		this.date = date;
	}

	public String getid() {
		return this.id;
	}

	public void setid(String id) {
		this.id = id;
	}

}
