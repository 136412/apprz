package spbstu.admin;

public class UserRegistration extends User {
	private String educationalInstitution;
	private String contacts;
	private String applicationDate;

	UserRegistration(String id, String fullName, String eMail,
			String educationalInstitution, String contacts, String applicationDate) {
		super(id, fullName, eMail, null, null);
		this.educationalInstitution = educationalInstitution;
		this.contacts = contacts;
		this.applicationDate = applicationDate;
	}
	
	public String geteducationalInstitution() {
		return educationalInstitution;
	}
	
	public void seteducationalInstitution(String educationalInstitution) {
		this.educationalInstitution = educationalInstitution;
	}
	
	public String getcontacts() {
		return contacts;
	}
	
	public void setcontacts(String contacts) {
		this.contacts = contacts;
	}
	
	public String getapplicationDate() {
		return applicationDate;
	}
	
	public void setapplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}
}
