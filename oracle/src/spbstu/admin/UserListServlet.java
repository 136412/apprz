package spbstu.admin;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public ArrayList<User> getZZZ(){
        ArrayList<User> v = new ArrayList<User>();
        try{
        	User d1 = new User();
        	User d2 = new User();
             d1.setfullName("Ab");
             d1.seteMail("abc@gmail.com");
             d1.settype("student");
             d1.setdate("01.01.2017");
             d1.setid("1");
             
             d2.setfullName("adad");
             d2.seteMail("abc@gmail.com");
             d2.settype("student");
             d2.setdate("01.01.2017");
             d2.setid("2");
            
             v.add(d1);
             v.add(d2);
        } catch(Exception asd){
            System.out.println(asd.getMessage());
        }
        return v;
    }

	public ArrayList<UserRegistration> getUsersRegistration(){
        ArrayList<UserRegistration> v = new ArrayList<UserRegistration>();
        try{
        	UserRegistration reg1 = new UserRegistration(
        			"1",
        			"Иван Иванович Петров",
        			"Учебный тест",
        			"spbtu",
        			"01",
        			"01.01.2017");
             v.add(reg1);
        } catch(Exception asd){
            System.out.println(asd.getMessage());
        }
        return v;
    }
	
	public ArrayList<UserDelete> getDeletedUsers(){
        ArrayList<UserDelete> v = new ArrayList<UserDelete>();
        try{
        	UserDelete d1 = new UserDelete();
        	UserDelete d2 = new UserDelete();
             d1.setfullName("Ab");
             d1.seteMail("abc@gmail.com");
             d1.settype("student");
             d1.setdate("01.01.2017");
             d1.setid("1");
             d1.setinSystem("6 years");
             
             d2.setfullName("adad");
             d2.seteMail("abc@gmail.com");
             d2.settype("student");
             d2.setdate("01.01.2017");
             d2.setid("2");
             d2.setinSystem("4 years");
            
             v.add(d1);
             v.add(d2);
        } catch(Exception asd){
            System.out.println(asd.getMessage());
        }
        return v;
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("users", getZZZ());
		request.setAttribute("usersReg", getUsersRegistration());
		request.setAttribute("usersDel", getDeletedUsers());
		request.setAttribute("admin_name", "Rect");
        RequestDispatcher view = request.getRequestDispatcher("/admin_form.jsp");
        view.forward(request, response);
	}
}