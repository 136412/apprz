package spbstu.admin;

public class UserDelete implements Cloneable {

	private String id;
	private String fullName;
	private String eMail;
	private String type;
	private String date;
	private String inSystem;

	public String getfullName() {
		return this.fullName;
	}

	public void setfullName(String fullName) {
		this.fullName = fullName;
	}

	public String geteMail() {
		return this.eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String gettype() {
		return this.type;
	}

	public void settype(String type) {
		this.type = type;
	}

	public String getdate() {
		return this.date;
	}

	public void setdate(String date) {
		this.date = date;
	}

	public String getid() {
		return this.id;
	}

	public void setid(String id) {
		this.id = id;
	}

	
	public String getinSystem() {
		return this.inSystem;
	}

	public void setinSystem(String inSystem) {
		this.inSystem = inSystem;
	}
}
