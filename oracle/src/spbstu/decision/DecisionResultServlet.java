package spbstu.decision;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DecisionResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public DecisionResultServlet(){
        super();
    }

	public ArrayList<TaskClass> getTasks() {
		ArrayList<TaskClass> result = new ArrayList<>();
		try {
			result.add(new TaskClass("1", "Упростите", "a=0\n3=4", "<p style=\"color:#FF0000\";>3=4</p>", "0"));
			result.add(new TaskClass("2", "Упростите", "a=1\n1=2", "", "0"));
			result.add(new TaskClass("3", "Упростите", "a=0\n3=4", "", "0"));
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return result;
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("my_test", getTasks());
		request.setAttribute("student_name", "Иванов Иван Иванович");
		RequestDispatcher view = request.getRequestDispatcher("/decision_result_form.jsp");
		view.forward(request, response);
	}
}
