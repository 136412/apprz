package spbstu.decision;

public class TaskClass {
	private String id;
	private String task;
	private String solution;
	private String error;
	private String answer;

	public TaskClass() {
	}

	public TaskClass(String id, String task) {
		this.setId(id);
		this.setTask(task);
	}

	public TaskClass(String id, String task, String solution, String error, String answer) {
		this.setId(id);
		this.setTask(task);
		this.solution = solution;
		this.error = error;
		this.answer = answer;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTask() {
		return task;
	}

	public void setTask(String task) {
		this.task = task;
	}

	public String getSolution() {
		return solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}
}

