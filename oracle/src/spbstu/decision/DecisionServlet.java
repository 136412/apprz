package spbstu.decision;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * Servlet implementation class DecisionServlet
 */
@WebServlet("/DecisionServlet")
public class DecisionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DecisionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    public ArrayList<TaskClass> getTasks() {
        ArrayList<TaskClass> result = new ArrayList<>();
        try{
        	result.add(new TaskClass("1",
        			"Упростите"));
        	result.add(new TaskClass("2",
        			"Докажите"));
        	result.add(new TaskClass("3",
        			"Упростите данное выражение"));
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
        return result;
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setAttribute("my_test", getTasks());
		request.setAttribute("student_id", 40);
		request.setAttribute("test_id", 3);
		request.setAttribute("student_name", "Иванов Иван Иванович");
		request.setAttribute("test_lenght", 50000); // ms , если test_lenght==0 то это учебный тест
        RequestDispatcher view = request.getRequestDispatcher("/decision_form.jsp");
        view.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
