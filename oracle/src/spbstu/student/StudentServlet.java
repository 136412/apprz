package spbstu.student;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import spbstu.admin.User;

public class StudentServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private User student;
	private ArrayList<Test> repoTest;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		student = new User(
		"1",
		"Петрова Ольга М",
		"abc@gmail.com",
		"student",
		"01.01.2017");
		createTests();
		request.setAttribute("student_id", 40);
		request.setAttribute("student_name", student.getfullName());
		request.setAttribute("assignedTests", getAssignedTests());
		request.setAttribute("availableTests", getAvailableTests());
		request.setAttribute("resultsOfTheTests", getResultsOfTheTests());
		request.setAttribute("evaluateTests", getEvaluateTests());
        RequestDispatcher view = request.getRequestDispatcher("/student_form.jsp");
        view.forward(request, response);
	}
	

	private void createTests() {
		repoTest = new ArrayList<>();

		Test test_1 = new Test(
    			"1",
    			"Тест 1",
    			"Пупкин Иван Иванович",
    			"учебный тест",
    			"2",
    			"100",
    			"0");

    	Test test_2 = new Test(
    			"2",
    			"Тест 2",
    			"Пышкин Иван Иванович",
    			"учебный тест",
    			"2",
    			"50",
    			"0");

    	Test test_3 = new Test(
    			"3",
    			"Тест 3",
    			"Пышкин Иван Иванович",
    			"контрольная работа",
    			"2",
    			"50",
    			"0");
    	
    	Test test_4 = new Test(
    			"4",
    			"Тест 4",
    			"Пышкин Иван Иванович",
    			"учебный тест",
    			"2",
    			"50",
    			"0");
    	
    	repoTest.add(test_1);		
    	repoTest.add(test_2);		
    	repoTest.add(test_3);		
    	repoTest.add(test_4);		
	}
	
	public ArrayList<Test> getAssignedTests() {
        ArrayList<Test> result = new ArrayList<>();
        try{
        	result.add(repoTest.get(0));
        	result.add(repoTest.get(1));
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
        return result;
    }
	
	public ArrayList<Test> getAvailableTests() {
        ArrayList<Test> result = new ArrayList<>();
        try{
        	result.add(repoTest.get(2));
        	result.add(repoTest.get(3));
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
        return result;
    }
	
	public ArrayList<Test> getResultsOfTheTests() {
        ArrayList<Test> result = new ArrayList<>();
        try{
        	result.add(repoTest.get(0));
        	result.add(repoTest.get(1));
        	result.add(repoTest.get(2));
        	result.add(repoTest.get(3));
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
        return result;
    }

	public ArrayList<Test> getEvaluateTests() {
        ArrayList<Test> result = new ArrayList<>();
        try{
        	result.add(repoTest.get(0));
        	result.add(repoTest.get(1));
        	result.add(repoTest.get(2));
        	result.add(repoTest.get(3));
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
        return result;
    }
}
