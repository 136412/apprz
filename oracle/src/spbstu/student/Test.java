package spbstu.student;

public class Test {
	private String id;
	private String name;
	private String author;
	private String type;
	private String leadTime;
	private String points;
	private String mark;

	public Test() {
	}

	public Test(String id, String name, String author, String type, String leadTime, String points, String mark) {
		this.id = id;
		this.name = name;
		this.author = author;
		this.type = type;
		this.leadTime = leadTime;
		this.points = points;
		this.mark = mark;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLeadTime() {
		return leadTime;
	}

	public void setLeadTime(String leadTime) {
		this.leadTime = leadTime;
	}

	public String getPoints() {
		return points;
	}

	public void setPoints(String points) {
		this.points = points;
	}

	public String getMark() {
		return mark;
	}

	public void setMark(String mark) {
		this.mark = mark;
	}
}
