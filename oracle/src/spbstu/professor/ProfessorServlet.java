package spbstu.professor;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ProfessorServlet extends HttpServlet  {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private ArrayList<Assessment> repoTest;
	
    public ProfessorServlet() {
        super();
    }
    
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		createTests();


		request.setAttribute("myTests", getMyTests());
		request.setAttribute("statisticsTests", getStatisticsTests());
		
		request.setAttribute("usersProff", getUsers());
		request.setAttribute("tasks", getTasks());
		request.setAttribute("testStatsTasks", getTestStatistics());
		request.setAttribute("themes", getThemes());
		request.setAttribute("proff_name", "Никифоров Николай Валерьевич");
		request.setAttribute("groups", getGroups());
		request.setAttribute("proff_id", 1);
		RequestDispatcher view = request.getRequestDispatcher("/professor.jsp");
        view.forward(request, response);
	}
    
    private ArrayList<Group> getGroups() {
    	ArrayList<Group> result = new ArrayList<>();

    	Group group_1 = new Group("1", "111111/1");
    	Group group_2 = new Group("2", "222222/2");
    	Group group_3 = new Group("3", "333333/3");
    	Group group_4 = new Group("4", "444444/4");

    	result.add(group_1);		
    	result.add(group_2);		
    	result.add(group_3);		
    	result.add(group_4);
    	
    	return result;
    }

	
	private void createTests() {
		repoTest = new ArrayList<>();

		Assessment test_1 = new Assessment(
    			"1",
    			"Тест 1",
    			"Учебный тест",
    			50,
    			100,
    			0,
    			5.6);

		Assessment test_2 = new Assessment(
    			"2",
    			"Тест 2",
    			"Учебный тест",
    			50,
    			100,
    			0,
    			5.6);

		Assessment test_3 = new Assessment(
    			"3",
    			"Тест 3",
    			"Учебный тест",
    			50,
    			100,
    			0,
    			5.6);

		Assessment test_4 = new Assessment(
    			"4",
    			"Тест 4",
    			"Учебный тест",
    			50,
    			100,
    			0,
    			5.6);
    	
    	repoTest.add(test_1);		
    	repoTest.add(test_2);		
    	repoTest.add(test_3);		
    	repoTest.add(test_4);		
	}
	
	public ArrayList<Assessment> getMyTests() {
        ArrayList<Assessment> result = new ArrayList<>();
        try{
        	result.add(repoTest.get(0));
        	result.add(repoTest.get(1));
        	result.add(repoTest.get(2));
        	result.add(repoTest.get(3));
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
        return result;
    }
	
	
	
	public ArrayList<Assessment> getStatisticsTests() {
        ArrayList<Assessment> result = new ArrayList<>();
        try{
        	result.add(repoTest.get(0));
        	result.add(repoTest.get(1));
        	result.add(repoTest.get(2));
        	result.add(repoTest.get(3));
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
        return result;
    }	
	public ArrayList<Task> getTasksInTest() {
        ArrayList<Task> result = new ArrayList<>();
        try{
        	result.add(new Task("1",
        			"Задача с яблоками",
        			"Арифметика",
        			"1",
        			"Иван Иванович",
        			"10",
        			"Сколько ребер в графе?",
        			"30"));
        	result.add(new Task("2",
        			"Задача К",
        			"Кодирование",
        			"3",
        			"Иван Иванович",
        			"10",
        			"Сколько ребер в мире?",
        			"Много"));
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
        return result;
    }

	public ArrayList<Theme> getThemes() {
        ArrayList<Theme> result = new ArrayList<>();
        try{
        	result.add(new Theme("1",
        			"Арифметика"));
        	result.add(new Theme("2",
        			"Кодирование"));
        	result.add(new Theme("3",
        			"Комбинаторика"));
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
        return result;
    }
	public ArrayList<Task> getTasks() {
        ArrayList<Task> result = new ArrayList<>();
        try{
        	result.add(new Task("1",
        			"Задача с яблоками",
        			"Арифметика",
        			"1",
        			"Иван Иванович",
        			"10",
        			"Сколько ребер в мире?",
        			"Много"));
        	result.add(new Task("2",
        			"Задача К",
        			"Кодирование",
        			"3",
        			"Никифоров Николай Валерьевич",
        			"1",
        			"Сколько ребер в мире?",
        			"Много"));
        	result.add(new Task("3",
        			"Задача КA",
        			"Булева алгебра",
        			"2",
        			"Брен",
        			"5",
        			"Сколько ребер в мире?",
        			"Много"));
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
        return result;
    }
	
	public ArrayList<UserStudents> getUsers() {
        ArrayList<UserStudents> result = new ArrayList<>();
        try{
        	result.add(new UserStudents("1",
        			"Никифоров Николай Валерьевич",
        			"преподаватель",
        			"-",
        			"",
        			"",
        			"",
        			"",
        			""));
        	result.add(new UserStudents("2",
        			"Павлова Полина Сергеевна",
        			"студент",
        			"1515/2",
        			"",
        			"",
        			"",
        			"",
        			""));
        	result.add(new UserStudents("3",
        			"Ильин Илья Иванович",
        			"студент",
        			"6565/1",
        			"",
        			"",
        			"",
        			"",
        			""));
        } catch(Exception e){
            System.out.println(e.getMessage());
        }
        return result;
    }
	public ArrayList<UserStudents> getTestStatistics() {
        ArrayList<UserStudents> result = new ArrayList<>();
        try{
        	result.add(new UserStudents("2",
        			"Павлова Полина Сергеевна",
        			"",
        			"1515/2",
        			"10",
        			"25",
        			"55",
        			"55",
        			"1"));
        	result.add(new UserStudents("3",
        			"Ильин Илья Иванович",
        			"",
        			"6565/1",
        			"15",
        			"35",
        			"50",
        			"50",
        			"1"));
        	result.add(new UserStudents("3",
        			"Ильин Илья Иванович",
        			"",
        			"6565/1",
        			"25",
        			"45",
        			"60",
        			"60",
        			"2"));
        	
        	
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }
        return result;
    }
}
