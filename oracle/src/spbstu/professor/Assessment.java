package spbstu.professor;

public class Assessment {
	private String id;
	private String name;
	private String type;

	private double gradePointAverage;
	private double gradePointMinimum;
	private double gradePointMaximum;

	private double mark;

	public Assessment() {
	}
	
	public Assessment(String id, String name, String type, double gradePointAverage, double gradePointMinimum,
			double gradePointMaximum, double mark) {
		this.id = id;
		this.name = name;
		this.type = type;
		this.gradePointAverage = gradePointAverage;
		this.gradePointMinimum = gradePointMinimum;
		this.gradePointMaximum = gradePointMaximum;
		this.mark = mark;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getGradePointAverage() {
		return gradePointAverage;
	}

	public void setGradePointAverage(double gradePointAverage) {
		this.gradePointAverage = gradePointAverage;
	}

	public double getGradePointMinimum() {
		return gradePointMinimum;
	}

	public void setGradePointMinimum(double gradePointMinimum) {
		this.gradePointMinimum = gradePointMinimum;
	}

	public double getGradePointMaximum() {
		return gradePointMaximum;
	}

	public void setGradePointMaximum(double gradePointMaximum) {
		this.gradePointMaximum = gradePointMaximum;
	}

	public double getMark() {
		return mark;
	}

	public void setMark(double mark) {
		this.mark = mark;
	}
}
