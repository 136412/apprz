package spbstu.professor;


public class Task {
	private String id;
	private String name;
	private String theme;
	private String difficulty;
	private String author;
	private String author_id;
	private String problem;
	private String answer;
	
	public Task() {
	}

	public Task(String id, String name, String theme, String difficulty, String author, String author_id, String problem, String answer) {
		this.id = id;
		this.name = name;
		this.theme = theme;
		this.difficulty = difficulty;
		this.author = author;
		this.author_id = author_id;
		this.problem = problem;
		this.answer = answer;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}
	
	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}
	
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getProblem() {
		return problem;
	}

	public void setProblem(String problem) {
		this.problem = problem;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getAuthor_id() {
		return author_id;
	}

	public void setAuthor_id(String author_id) {
		this.author_id = author_id;
	}
}
