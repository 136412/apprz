package spbstu.professor;


public class UserStudents {
	private String id;
	private String name;
	private String type;
	private String group;
	private String count;
	private String first;
	private String last;
	private String result;
	private String testid;
	public UserStudents() {
	}

	public UserStudents(String id, String name, String type, String group, String count, String first, String last, String result, String testid) {
		this.id = id;
		this.name = name;
		this.type = type;
		this.group = group;
		this.count = count;
		this.first = first;
		this.last = last;
		this.result = result;
		this.testid = testid;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}
	
	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}
	
	public String getFirst() {
		return first;
	}

	public void setFirst(String first) {
		this.first = first;
	}
	public String getLast() {
		return last;
	}

	public void setLast(String last) {
		this.last = last;
	}
	
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getTestid() {
		return testid;
	}

	public void setTestid(String testid) {
		this.testid = testid;
	}
}
