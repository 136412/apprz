<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Oracle</title>

<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/imagehover.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">


<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
<link rel="stylesheet" href="dist/css/bootstrap-select.css">

<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"
	type="text/javascript"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"
	type="text/javascript"></script>
<script src="dist/js/bootstrap-select.js" type="text/javascript"></script>

</head>

<body>

	<script type="text/javascript">
					
					function testOnChange() {
						$rowsNo = $('#testResults tbody tr').filter(function () {
							return true
						}).show();
						var $rowsNo = $('#testResults tbody tr').filter(function () {
							var e = document.getElementById("testResultSelect");
							var strUser = e.options[e.selectedIndex].value;
							return $.trim($(this).find('td').eq(0).text()) != strUser;
						}).hide();
					}
					function redirectTestStat(test_id, test_name) {
						var $rowsNo = $('#teststattable tbody tr').filter(function () {
							return true
						}).show();
						var $rowsNo2 = $('#teststattable tbody tr').filter(function () {
							
							return $.trim($(this).find('td').eq(0).text()) != test_id;
							
						}).hide();
						document.getElementById("teststatname").innerHTML = test_name;
						document.getElementById("redirectTo2c-5").click();
					}
					function checkIfNewTaskIsFilled() {
						if (!$('input[name=taskname]').val() || !$('input[name=taskDef]').val() || !$('input[name=taskanswer]').val()) {
							$('#error-new-task').modal('show');
						} else {
							$('form#new-task').submit();
						}
					}
					function checkIfNewTestIsFilled() {
						if (!$('input[name=testname]').val()) {
							$('#error-new-test-2').modal('show');
						} else {
							var ids = $('.tasktestids input:checked').map(function () {
				                return this.value;
				            }).get();
							console.log(ids);
							if (ids.length == 0) {
								$("#error-new-test-2").modal();
							} else {
								$('form#new-test').submit();
							}
						}
					}
					function checkIfNewGroupIsFilled() {
						if (!$('input[name=groupname]').val()) {
							$('#error_group_2').modal('show');
						} else {
							var ids = $('.groupstudids input:checked').map(function () {
				                return this.value;
				            }).get();
							console.log(ids);
							if (ids.length == 0) {
								$('#error_group_2').modal('show');
							} else {
								$('form#new-group').submit();
							}
						}
					}
					
					function showAllUsers() {
						var $rowsNo = $('#regUsers tbody tr').filter(function () {
								return true
							}).show();
					}
					
					function showCreationModal() {
						$('#creation-modal').modal('show');
					}

					function showStudUsers() {
						showAllUsers();
			        	var $rowsNo = $('#regUsers tbody tr').filter(function () {
								return $.trim($(this).find('td').eq(2).text()) != "студент"
							}).hide();
			        }

			        function showProffUsers() {
			        	showAllUsers();
			        	var $rowsNo = $('#regUsers tbody tr').filter(function () {
								return $.trim($(this).find('td').eq(2).text()) != "преподаватель"
							}).hide();
			        }
			        
			        function showAllTasks() {
						var $rowsNo = $('#taskTable tbody tr').filter(function () {
								return true
							}).show();
					}

					function showMyTasks() {
						showAllUsers();
			        	var $rowsNo = $('#taskTable tbody tr').filter(function () {
								return $.trim($(this).find('td').eq(0).text()) != <c:out value="${proff_id}" />;
							}).hide();
			        }
					function checkIfNewDialogIsFilled() {
						
						var ids = $('.appointinput input:checked').map(function () {
			                return this.value;
			            }).get();
						console.log(ids);
						if (ids.length == 0) {
							$('#error_appoint').modal('show');
						} else {
							$('form#appoint').submit();
						}
					}
					
					function appointModal(test_id) {
						document.getElementById("appointtestid").value = test_id;
						$('#select_test').modal('show');
					}
					
				</script>
		




	<!--Navigation bar-->
	<nav class="navbar navbar-default navbar-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#myNavbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.html">Ora<span>cle</span></a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav navbar-right">
				<li><a onclick="showCreationModal();">Создать</a></li>
				<li class="btn-trial"><a href="#" data-target="#login"
					data-toggle="modal">Выйти из системы</a></li>
			</ul>
		</div>
	</div>
	</nav>
	<!--/ Navigation bar-->


	<div class="modal fade" id="select_test" role="dialog" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog modal-lm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-body padtrbl">
					<div class="modal-body padtrbl">

						<div class="loginmodal-container">
							<h3>Выберите, на кого хотите назначить тест</h3>
							<form action="PrintReq" id="appoint" method="POST">

								<div class="appointinput"
									style="height: 150px; width: 400px; position: relative; max-height: 100%; overflow: auto; border: 1px solid black;">
									<div class="itemconfiguration"
										style="padding-left: 30px; overflow-y: auto;">


										<c:forEach var="group" items="${groups}">
											<input type="checkbox" name="checkbox_groups"
												value="${group.id}" /> ${group.name}<br />
										</c:forEach>

										
										<c:forEach var="user" items="${usersProff}">
											<c:if test="${user.type == 'студент'}">
												<input type="checkbox" name="checkbox_students"
													value="${user.id}" /> ${user.name}<br />
											</c:if>
										</c:forEach>
									</div>
								</div>
								<h3></h3>
								<div class="btn-toolbar" role="toolbar"
									aria-label="Toolbar with button groups">
									<input type="hidden" name="taskId"
										id="appointtestid" />
									<div class="btn-group mr-2" role="group">
										<button type="button" class="btn btn-green" onclick="checkIfNewDialogIsFilled();">Назначить</button>
									</div>
									<div class="btn-group mr-2" role="group">
										<button type="button" class="btn btn-green" data-dismiss="modal">Отмена</button>
									</div>
								</div>

							</form>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="container">
		<div>
			Вы зашли в систему как: <label for="name"><c:out
					value="${proff_name}" /></label>
		</div>
	</div>

	<div id="exTab1" class="container">
		<ul class="nav nav-pills">
			<%
				if (request.getParameter("tabID") == null || request.getParameter("tabID").equals("1a")) {
			%>
			<li class="active"><a href="#1a" data-toggle="tab">Мои тесты</a></li>
			<li><a href="#2a" data-toggle="tab">Результаты тестов</a></li>
			<li><a href="#3a" data-toggle="tab">Статистика</a></li>
			<li><a href="#4a" data-toggle="tab">Задачи</a></li>
			<li><a href="#5a" data-toggle="tab">Все пользователи</a></li>
			<%
				} else if (request.getParameter("tabID").equals("2a")) {
			%>
			<li><a href="#1a" data-toggle="tab">Мои тесты</a></li>
			<li class="active"><a href="#2a" data-toggle="tab">Результаты
					тестов</a></li>
			<li><a href="#3a" data-toggle="tab">Статистика</a></li>
			<li><a href="#4a" data-toggle="tab">Задачи</a></li>
			<li><a href="#5a" data-toggle="tab">Все пользователи</a></li>
			<%
				} else if (request.getParameter("tabID").equals("3a")) {
			%>
			<li><a href="#1a" data-toggle="tab">Мои тесты</a></li>
			<li><a href="#2a" data-toggle="tab">Результаты тестов</a></li>
			<li class="active"><a href="#3a" data-toggle="tab">Статистика</a></li>
			<li><a href="#4a" data-toggle="tab">Задачи</a></li>
			<li><a href="#5a" data-toggle="tab">Все пользователи</a></li>
			<%
				} else if (request.getParameter("tabID").equals("4a")) {
			%>
			<li><a href="#1a" data-toggle="tab">Мои тесты</a></li>
			<li><a href="#2a" data-toggle="tab">Результаты тестов</a></li>
			<li><a href="#3a" data-toggle="tab">Статистика</a></li>
			<li class="active"><a href="#4a" data-toggle="tab">Задачи</a></li>
			<li><a href="#5a" data-toggle="tab">Все пользователи</a></li>
			<%
				} else if (request.getParameter("tabID").equals("5a")) {
			%>
			<li><a href="#1a" data-toggle="tab">Мои тесты</a></li>
			<li><a href="#2a" data-toggle="tab">Результаты тестов</a></li>
			<li><a href="#3a" data-toggle="tab">Статистика</a></li>
			<li><a href="#4a" data-toggle="tab">Задачи</a></li>
			<li class="active"><a href="#5a" data-toggle="tab">Все
					пользователи</a></li>
			<%
				} else {
			%>
			<li><a href="#1a" data-toggle="tab">Мои тесты</a></li>
			<li><a href="#2a" data-toggle="tab">Результаты тестов</a></li>
			<li><a href="#3a" data-toggle="tab">Статистика</a></li>
			<li><a href="#4a" data-toggle="tab">Задачи</a></li>
			<li><a href="#5a" data-toggle="tab">Все пользователи</a></li>
			<%
				}
			%>
		</ul>

		<div class="tab-content clearfix">
			<%
				if (request.getParameter("tabID") == null || request.getParameter("tabID").equals("1a")) {
			%>
			<div class="tab-pane active" id="1a">
				<%
					} else {
				%>
				<div class="tab-pane" id="1a">
					<%
						}
					%>
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Название</th>
								<th>Тип теста</th>
								<th>Оценка теста</th>
								<th>Подробнее</th>
								<th>Назначение</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="test" items="${myTests}">
								<tr>
									<td>${test.id}</td>
									<td>${test.name}</td>
									<td>${test.type}</td>
									<td>${test.mark}</td>
									<td>
										<form action="RedirectServlet" method="POST">
											<input type="hidden" name="tabId" value="2c-1"> <input
												type="hidden" name="testId"
												value='<c:out value="${test.id}"/>'>
											<button type="submit" class="btn btn-green">Подробнее</button>
										</form> <!--
										<button type="button" class="btn btn-green" data-toggle="tab"
											value='<c:out value="${test.id}"/>' href="#2c-1">Подробнее</button>
										-->
									</td>
									<td>
										<!--
										<form action="RedirectServlet" method="POST">
											<input type="hidden" name="testId"
												value="<c:out value="${test.id}"/>">
											<button type="submit" class="btn btn-green">Назначить</button>
										</form>
										-->
										<button class="btn btn-green btn-space" type="button"
											
											onclick="appointModal(<c:out value="${test.id}"/>);">Назначить</button>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>

				<%
					if (request.getParameter("tabID") != null && request.getParameter("tabID").equals("2c-1")) {
				%>
				<div class="tab-pane active" id="2c-1">
					<%
						} else {
					%>
					<div class="tab-pane" id="2c-1">
						<%
							}
						%>
						<div class="modal-body padtrbl">
							<div class="loginmodal-container">
								<div class="form-group row">
									<label class="col-sm-2">Тест</label>
									<div class="col-sm-10">
										<div class="form-check">
											<label class="form-check-label"><c:out value="${gTestName}"/></label>
										</div>
									</div>
								</div>

								<div class="form-group row">
									<label class="col-sm-2">Тип теста</label>
									<div class="col-sm-10">
										<div class="form-check">
											<label class="form-check-label"><c:out value="${gTestTheme}"/></label>
										</div>
									</div>
								</div>

								Задачи в составе теста

								<table class="table">
									<thead>
										<tr>
											<th>#</th>
											<th>Название</th>
											<th>Тема</th>
											<th>Сложность</th>
											<th>Автор</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<c:forEach var="tintest" items="${tasksInTest}">
											<tr>
												<td>${tintest.id}</td>
												<td>${tintest.name}</td>
												<td>${tintest.theme}</td>
												<td>${tintest.difficulty}</td>
												<td>${tintest.author}</td>
												<!-- 
													<td>
														<button type="button" class="btn btn-green btn-space"
															data-toggle="tab" href="#2c-2">Подробнее</button>
													</td>

														<form action="PrintReq" method="POST">
															<input type="hidden" name="testId" value="<c:out value="${tintest.id}"/>">
															<button type="submit" class="btn btn-green">Подробнее</button>
														</form>
													 -->

												<td>
													<form action="RedirectServlet" method="POST">
														<input type="hidden" name="tabId" value="2c-2"> <input
															type="hidden" name="taskId"
															value='<c:out value="${tintest.id}"/>'>
														<button type="submit" class="btn btn-green">Подробнее</button>
													</form>
												</td>


											</tr>
										</c:forEach>
									</tbody>
								</table>

								<div class="btn-toolbar" role="toolbar"
									aria-label="Toolbar with button groups">
									<div class="btn-group mr-2" role="group">
										<button type="button" class="btn btn-green btn-space"
											data-toggle="tab" href="#" data-target="#1a">Список
											тестов</button>
									</div>
									<!--
										<div class="btn-group mr-2" role="group">
											<button type="button" class="btn btn-green">Удалить
												тест</button>
										</div>
										-->

									<form action="PrintReq" method="POST">
										<input type="hidden" name="testId"
											value='<c:out value="${gTestId}"/>'>
										<button type="submit" class="btn btn-green">Удалить
											тест</button>
									</form>
								</div>

							</div>
						</div>
					</div>


					<%
						if (request.getParameter("tabID") != null && request.getParameter("tabID").equals("2c-2")) {
					%>
					<div class="tab-pane active" id="2c-2">
						<%
							} else {
						%>
						<div class="tab-pane" id="2c-2">
							<%
								}
							%>
							<div class="modal-body padtrbl">
								<div class="loginmodal-container">
									<form action="PrintReq" method="POST">
										<div class="form-group row">
											<label class="col-sm-2">Задача <c:out
													value="${gTask.id}" /></label>
											<div class="col-sm-10">
												<div class="form-check">
													<label class="form-check-label"><c:out
															value="${gTask.name}" /></label>
												</div>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-sm-2">Тема задачи</label>
											<div class="col-sm-10">
												<div class="form-check">
													<label class="form-check-label"><c:out
															value="${gTask.theme}" /></label>
												</div>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-sm-2">Сложность</label>
											<div class="col-sm-10">
												<div class="form-check">
													<label class="form-check-label"><c:out
															value="${gTask.difficulty}" /></label>
												</div>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-sm-2">Автор</label>
											<div class="col-sm-10">
												<div class="form-check">
													<label class="form-check-label"><c:out
															value="${gTask.author}" /></label>
												</div>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-sm-2">Условие</label>
											<div class="col-sm-10">
												<div class="form-check">
													<label class="form-check-label"><c:out
															value="${gTask.problem}" /></label>
												</div>
											</div>
										</div>

										<div class="form-group row">
											<label class="col-sm-2">Ответ</label>
											<div class="col-sm-10">
												<div class="form-check">
													<label class="form-check-label"><c:out
															value="${gTask.answer}" /></label>
												</div>
											</div>
										</div>

										<div class="btn-toolbar" role="toolbar"
											aria-label="Toolbar with button groups">
											<div class="btn-group mr-2" role="group">
												<button type="button" class="btn btn-green btn-space"
													data-toggle="tab" href="#1a">Список тестов</button>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>


						<%
							if (request.getParameter("tabID") != null && request.getParameter("tabID").equals("2c-3")) {
						%>
						<div class="tab-pane active" id="2c-3">
							<%
								} else {
							%>
							<div class="tab-pane" id="2c-3">
								<%
									}
								%>
								<div class="modal-body padtrbl">
									<div class="loginmodal-container">
										<form action="PrintReq" method="POST">
											<div class="form-group row">
												<label class="col-sm-2">Задача <c:out
														value="${gTask.id}" /></label>
												<div class="col-sm-10">
													<div class="form-check">
														<label class="form-check-label"><c:out
																value="${gTask.name}" /></label>
													</div>
												</div>
											</div>

											<div class="form-group row">
												<label class="col-sm-2">Тема задачи</label>
												<div class="col-sm-10">
													<div class="form-check">
														<label class="form-check-label"><c:out
																value="${gTask.theme}" /></label>
													</div>
												</div>
											</div>

											<div class="form-group row">
												<label class="col-sm-2">Сложность</label>
												<div class="col-sm-10">
													<div class="form-check">
														<label class="form-check-label"><c:out
																value="${gTask.difficulty}" /></label>
													</div>
												</div>
											</div>

											<div class="form-group row">
												<label class="col-sm-2">Автор</label>
												<div class="col-sm-10">
													<div class="form-check">
														<label class="form-check-label"><c:out
																value="${gTask.author}" /></label>
													</div>
												</div>
											</div>

											<div class="form-group row">
												<label class="col-sm-2">Условие</label>
												<div class="col-sm-10">
													<div class="form-check">
														<label class="form-check-label"><c:out
																value="${gTask.problem}" /></label>
													</div>
												</div>
											</div>

											<div class="form-group row">
												<label class="col-sm-2">Ответ</label>
												<div class="col-sm-10">
													<div class="form-check">
														<label class="form-check-label"><c:out
																value="${gTask.answer}" /></label>
													</div>
												</div>
											</div>
										</form>
										<div class="btn-toolbar" role="toolbar"
											aria-label="Toolbar with button groups">
											<div class="btn-group mr-2" role="group">
												<button type="button" class="btn btn-green btn-space"
													data-toggle="tab" href="#4a">Список задач</button>

											</div>
											<div class="btn-group mr-2" role="group">
												<form action="PrintReq" method="POST">
													<input type="hidden" name="testId"
														value='<c:out value="${gTask.id}"/>'>
													<button type="submit" class="btn btn-green">Удалить
														задачу</button>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>

							<%
								if (request.getParameter("tabID") != null && request.getParameter("tabID").equals("2a")) {
							%>
							<div class="tab-pane active" id="2a">
								<%
									} else {
								%>
								<div class="tab-pane" id="2a">
									<%
										}
									%>
									<div class="container">
										<h1></h1>
										<div class="form-group row">
											<p class="col-sm-2">Статистика по тесту</p>
											<div class="col-sm-10">
												<select id="testResultSelect" name="country"
													class="form-control selectpicker"
													onChange="testOnChange();">
													<option></option>
													<c:forEach var="test" items="${myTests}" varStatus="loopS">

														<option value='<c:out value="${test.id}" />'>${test.name}</option>

													</c:forEach>
												</select>
											</div>
										</div>

										<table class="table" id="testResults">
											<thead>
												<tr>
													<th>#</th>
													<th>Студент</th>
													<th>Группа</th>
													<th>Баллы</th>
													<th></th>
												</tr>
											</thead>
											<tbody>
												<c:forEach var="testres" items="${testStatsTasks}">
													<tr style="display: none;">
														<td style="display: none;">${testres.testid}</td>
														<td>${testres.id}</td>
														<td>${testres.name}</td>
														<td>${testres.group}</td>
														<td>${testres.result}</td>
														<td>
															<form action="PrintReq" method="POST">
																<input type="hidden" name="userTestId"
																	value='<c:out value="${testres.id}"/>'>
																<input type="hidden" name="testId"
																	value='<c:out value="${testres.testid}"/>'>
																<button type="submit" class="btn btn-green">Посмотреть решение</button>
															</form>
														</td>
													</tr>
												</c:forEach>

											</tbody>
										</table>
									</div>
								</div>

								

									<%
										if (request.getParameter("tabID") != null && request.getParameter("tabID").equals("3a")) {
									%>
									<div class="tab-pane active" id="3a">
										<%
											} else {
										%>
										<div class="tab-pane" id="3a">
											<%
												}
											%>
											<h1></h1>
											<div class="form-group row">
												<table class="table">
													<thead>
														<tr>
															<th>#</th>
															<th>Название теста</th>
															<th>Тип теста</th>
															<th>Средний балл</th>
															<th>Минимальный балл</th>
															<th>Максимальный балл</th>
															<th></th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="test" items="${statisticsTests}">
															<tr>
																<td>${test.id}</td>
																<td>${test.name}</td>
																<td>${test.type}</td>
																<td>${test.gradePointAverage}</td>
																<td>${test.gradePointMinimum}</td>
																<td>${test.gradePointMaximum}</td>
																<td>
																	<button type="button" class="btn btn-green btn-space"
																		onclick="javascript:redirectTestStat(<c:out value="${test.id}"/>, '<c:out value="${test.name}" />');">Подробнее</button>

																</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>
												<button type="button" id="redirectTo2c-5"
													class="btn btn-green btn-space" data-toggle="tab"
													href="#2c-5" style="display: none;"></button>
											</div>
										</div>

										<%
											if (request.getParameter("tabID") != null && request.getParameter("tabID").equals("2c-5")) {
										%>
										<div class="tab-pane active" id="2c-5">
											<%
												} else {
											%>
											<div class="tab-pane" id="2c-5">
												<%
													}
												%>
												<h1></h1>
												<div class="form-group row">
													<p class="col-sm-2">Статистика по тесту</p>
													<div class="col-sm-10">
														<label for="name" class="form-check-label"
															id="teststatname"></label>
													</div>
												</div>

												<table class="table" id="teststattable">
													<thead>
														<tr>
															<th>#</th>
															<th>Студент</th>
															<th>Группа</th>
															<th>Количество попыток</th>
															<th>Первая попытка</th>
															<th>Последняя попытка</th>
														</tr>
													</thead>
													<tbody>
														<c:forEach var="testst" items="${testStatsTasks}">
															<tr>
																<td style="display: none;">${testst.testid}</td>
																<td>${testst.id}</td>
																<td>${testst.name}</td>
																<td>${testst.group}</td>
																<td>${testst.count}</td>
																<td>${testst.first}</td>
																<td>${testst.last}</td>
															</tr>
														</c:forEach>
													</tbody>
												</table>

												<div class="btn-toolbar" role="toolbar"
													aria-label="Toolbar with button groups">
													<div class="btn-group mr-2" role="group">
														<button type="button" class="btn btn-green btn-space"
															data-toggle="tab" href="#3a">Статистика</button>
													</div>
												</div>
											</div>

											<%
												if (request.getParameter("tabID") != null && request.getParameter("tabID").equals("4a")) {
											%>
											<div class="tab-pane active" id="4a">
												<%
													} else {
												%>
												<div class="tab-pane" id="4a">
													<%
														}
													%>
													<form name="" id="" class="form-horizontal" action="">
														<div class="form-group">
															<div class="col-sm-3">
																<div class="btn-group" data-toggle="buttons">
																	<label class="btn btn-primary active" onclick="showAllTasks();"> <input
																		type="radio" name="options" id="option1"
																		autocomplete="off" checked> Все задачи
																	</label>
																	<label class="btn btn-primary" onclick="showMyTasks();"> <input
																		type="radio" name="options" id="option2"
																		autocomplete="off"> Мои задачи
																	</label>
																</div>
															</div>
															
														</div>
													</form>

													<table class="table" id="taskTable">
														<thead>
															<tr>
																<th>#</th>
																<th>Название</th>
																<th>Тема</th>
																<th>Сложность</th>
																<th>Автор</th>
																<th></th>
															</tr>
														</thead>
														<tbody>
															<c:forEach var="task" items="${tasks}">
																<tr>
																	<td style="display: none;">${task.author_id}</td>
																	<td>${task.id}</td>
																	<td>${task.name}</td>
																	<td>${task.theme}</td>
																	<td>${task.difficulty}</td>
																	<td>${task.author}</td>
																	<td>
																		<form action="RedirectServlet" method="POST">
																			<input type="hidden" name="tabId" value="2c-3">
																			<input type="hidden" name="taskId"
																				value='<c:out value="${task.id}"/>'>
																			<button type="submit" class="btn btn-green">Подробнее</button>
																		</form>
																	</td>
																</tr>
															</c:forEach>

														</tbody>
													</table>
												</div>


												<%
													if (request.getParameter("tabID") != null && request.getParameter("tabID").equals("5a")) {
												%>
												<div class="tab-pane active" id="5a">
													<%
														} else {
													%>
													<div class="tab-pane" id="5a">
														<%
															}
														%>
														<form name="" id="" class="form-horizontal" action="">
															<div class="form-group">
																<div class="col-sm-5">
																	<div class="btn-group" data-toggle="buttons">
																		<label class="btn btn-primary active"
																			onclick="showAllUsers();"> <input
																			type="radio" name="options" id="option1"
																			autocomplete="off" checked> Все пользователи
																		</label> <label class="btn btn-primary"
																			onclick="showProffUsers();"> <input
																			type="radio" name="options" id="option2"
																			autocomplete="off"> Только преподаватели
																		</label> <label class="btn btn-primary"
																			onclick="showStudUsers();"> <input
																			type="radio" name="options" id="option3"
																			autocomplete="off"> Только студенты
																		</label>
																	</div>
																</div>
															</div>
														</form>

														<table class="table" id="regUsers">
															<thead>
																<tr>
																	<th>#</th>
																	<th>ФИО</th>
																	<th>Тип пользователя</th>
																	<th>Группа</th>
																</tr>
															</thead>
															<tbody>
																<c:forEach var="usrs" items="${usersProff}">
																	<tr>
																		<td>${usrs.id}</td>
																		<td>${usrs.name}</td>
																		<td>${usrs.type}</td>
																		<td>${usrs.group}</td>
																	</tr>
																</c:forEach>
															</tbody>
														</table>
													</div>

												</div>

												<div class="modal fade" id="creation-modal" role="dialog">
													<div class="modal-dialog modal-lm">
														<!-- Modal content no 1-->
														<div class="modal-content">

															<div class="modal-body padtrbl">
																<div id="exTab1" class="container">
																	<ul class="nav nav-pills">
																		<li class="active"><a href="#1b"
																			data-toggle="tab">Задача</a></li>
																		<li><a href="#2b" data-toggle="tab">Тест</a></li>
																		<li><a href="#3b" data-toggle="tab">Группа
																				студентов</a></li>
																	</ul>

																	<div class="tab-content clearfix">
																		<div class="tab-pane active" id="1b">
																			<div class="modal-body padtrbl">

																				<div class="loginmodal-container">
																					<h3>Новая задача</h3>
																					<form action="PrintReq" id="new-task" method="POST">

																						<div class="form-group row">
																							<label class="col-sm-2">название</label>
																							<div class="col-sm-10">
																								<div class="form-check">
																									<label class="form-check-label"> <input
																										type="text" name="taskname">
																									</label>
																								</div>
																							</div>
																						</div>
																						<div class="form-group row">
																							<label class="col-sm-2">тема</label>
																							<div class="col-sm-10">
																								<div class="form-check">
																									<label class="form-check-label"> <select
																										class="form-control" name="tasktheme">
																											<c:forEach var="theme" items="${themes}">
																												<option value="${theme.id}">${theme.name}</option>
																											</c:forEach>
																									</select>
																									</label>
																								</div>
																							</div>
																						</div>
																						<div class="form-group row">
																							<label class="col-sm-2">сложность</label>
																							<div class="col-sm-10">
																								<div class="form-check">
																									<label class="form-check-label"> <select
																										class="form-control" name="taskdiff">
																											<option value="1">1</option>
																											<option value="2">2</option>
																											<option value="3">3</option>
																											<option value="4">4</option>
																											<option value="5">5</option>
																									</select>
																									</label>
																								</div>
																							</div>
																						</div>
																						<div class="form-group row">
																							<label class="col-sm-2">условие</label>
																							<div class="col-sm-10">
																								<div class="form-check">
																									<label class="form-check-label"> <input
																										type="text" name="taskDef">
																									</label>
																								</div>
																							</div>
																						</div>

																						<div class="form-group row">
																							<label class="col-sm-2">правильный ответ</label>
																							<div class="col-sm-10">
																								<div class="form-check">
																									<label class="form-check-label"> <input
																										type="text" name="taskanswer">
																									</label>
																								</div>
																							</div>
																						</div>
																						<div class="btn-toolbar" role="toolbar"
																							aria-label="Toolbar with button groups">
																							<div class="btn-group mr-2" role="group">
																								<button type="button"
																									onclick="checkIfNewTaskIsFilled();"
																									class="btn btn-green">Создать задачу</button>

																							</div>
																							<div class="btn-group mr-2" role="group">
																								<button type="button" class="btn btn-green"
																									data-dismiss="modal">Отмена</button>
																							</div>
																						</div>
																					</form>

																				</div>
																			</div>
																		</div>
																		<div class="tab-pane" id="2b">
																			<div class="modal-body padtrbl">

																				<div class="loginmodal-container">
																					<h3>Новый тест</h3>
																					<form action="PrintReq" id="new-test" method="POST">

																						<div class="form-group row">
																							<label class="col-sm-2">название</label>
																							<div class="col-sm-10">
																								<div class="form-check">
																									<label class="form-check-label"> <input
																										type="text" name="testname">
																									</label>
																								</div>
																							</div>
																						</div>
																						<div class="form-group row">
																							<label class="col-sm-2">тип теста</label>
																							<div class="col-sm-10">
																								<div class="form-check">
																									<label class="form-check-label"> <select
																										class="form-control" name="testtype">
																											<option value="training">учебный
																												тест</option>
																											<option value="exam">контрольная
																												работа</option>
																									</select>
																									</label>
																								</div>
																							</div>
																						</div>

																						<div class="form-group row">
																							<label class="col-sm-2">время работы</label>
																							<div class="col-sm-10">
																								<div class="form-check">
																									<label class="form-check-label"> <input
																										type="text" name="testtime">
																									</label>
																								</div>
																							</div>
																						</div>

																						<h4>Выберите задачи из списка</h4>



																						<div class="tasktestids"
																							style="height: 150px; width: 400px; position: relative; max-height: 100%; overflow: auto; border: 1px solid black;">

																							<div class="itemconfiguration"
																								style="padding-left: 30px; overflow-y: auto;">
																								<c:forEach var="task" items="${tasks}">
																									<input type="checkbox" name="testtaskid"
																										value="${task.id}" />${task.name}<br />
																								</c:forEach>
																							</div>
																						</div>
																						<h3></h3>
																						<div class="btn-toolbar" role="toolbar"
																							aria-label="Toolbar with button groups">
																							<div class="btn-group mr-2" role="group">
																								<button type="button"
																									onclick="checkIfNewTestIsFilled();"
																									class="btn btn-green">Создать тест</button>
																							</div>
																							<div class="btn-group mr-2" role="group">
																								<button type="button" class="btn btn-green"
																									data-dismiss="modal">Отмена</button>
																							</div>
																						</div>

																					</form>

																				</div>
																			</div>
																		</div>

																		<div class="tab-pane" id="3b">
																			<div class="modal-body padtrbl">

																				<div class="loginmodal-container">
																					<h3>Новая группа студентов</h3>
																					<form action="PrintReq" id="new-group"
																						method="POST">

																						<div class="form-group row">
																							<label class="col-sm-2">название</label>
																							<div class="col-sm-10">
																								<div class="form-check">
																									<label class="form-check-label"> <input
																										type="text" name="groupname">
																									</label>
																								</div>
																							</div>
																						</div>
																						<div class="form-group row">
																							<label class="col-sm-2">участники</label>
																						</div>

																						<div class="groupstudids"
																							style="height: 150px; width: 400px; position: relative; max-height: 100%; overflow: auto; border: 1px solid black;">

																							<div class="itemconfiguration"
																								style="padding-left: 30px; overflow-y: auto;">
																								<c:forEach var="user" items="${usersProff}">
																									<c:if test="${user.type == 'студент'}">
																										<input type="checkbox" name="groupstudentid"
																											value='<c:out value="${user.id}" />' /> ${user.name}<br />
																									</c:if>
																								</c:forEach>
																							</div>
																						</div>
																						<h3></h3>
																						<div class="btn-toolbar" role="toolbar"
																							aria-label="Toolbar with button groups">
																							<div class="btn-group mr-2" role="group">
																								<button type="button"
																									onclick="checkIfNewGroupIsFilled();"
																									class="btn btn-green">Создать группу</button>
																							</div>
																							<div class="btn-group mr-2" role="group">
																								<button type="button" class="btn btn-green"
																									data-dismiss="modal">Отмена</button>
																							</div>
																						</div>


																					</form>

																				</div>
																			</div>
																		</div>

																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>

											<div class="modal fade" id="error-new-task" role="dialog">
												<div class="modal-dialog modal-sm">
													<!-- Modal content no 1-->
													<div class="modal-content">
														<div class="modal-header">
															<h4 class="modal-title text-center form-title">Ошибка</h4>
														</div>
														<div class="modal-body padtrbl">
															<div class="login-box-body">
																<p class="login-box-msg">Все поля обязательны для
																	заполнения</p>
																<div class="form-group">


																	<div class="row">
																		<div class="col-xs-12">
																			<button type="submit"
																				class="btn btn-green btn-block btn-flat"
																				data-dismiss="modal">OK</button>
																		</div>
																	</div>

																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="modal fade" id="confirm-new-task" role="dialog">
											<div class="modal-dialog modal-sm">
												<!-- Modal content no 1-->
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title text-center form-title">Поздравляем</h4>
													</div>
													<div class="modal-body padtrbl">
														<div class="login-box-body">
															<p class="login-box-msg">Новая задача была создана и
																успешно сохранена в системе</p>
															<div class="form-group">
																<form action="PrintReq" id="loginForm" method="POST">
																	<div class="form-group has-feedback"></div>
																	<div class="form-group has-feedback"></div>

																	<div class="row">
																		<div class="col-xs-12">
																			<button type="submit"
																				class="btn btn-green btn-block btn-flat">OK</button>
																		</div>
																	</div>
																</form>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="modal fade" id="error-new-test-1" role="dialog">
											<div class="modal-dialog modal-sm">
												<!-- Modal content no 1-->
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title text-center form-title">Ошибка</h4>
													</div>
													<div class="modal-body padtrbl">
														<div class="login-box-body">
															<p class="login-box-msg">Тест с таким названием уже
																существует в системе.</p>
															<div class="form-group">
																<form action="PrintReq" id="loginForm" method="POST">
																	<div class="form-group has-feedback"></div>
																	<div class="form-group has-feedback"></div>

																	<div class="row">
																		<div class="col-xs-12">
																			<button type="submit"
																				class="btn btn-green btn-block btn-flat">OK</button>
																		</div>
																	</div>
																</form>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="modal fade" id="error-new-test-2" role="dialog">
											<div class="modal-dialog modal-sm">
												<!-- Modal content no 1-->
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title text-center form-title">Ошибка</h4>
													</div>
													<div class="modal-body padtrbl">
														<div class="login-box-body">
															<p class="login-box-msg">Заполнены не все поля</p>
															<div class="form-group">

																<div class="form-group has-feedback"></div>
																<div class="form-group has-feedback"></div>

																<div class="row">
																	<div class="col-xs-12">
																		<button type="submit"
																			class="btn btn-green btn-block btn-flat"
																			data-dismiss="modal">OK</button>
																	</div>
																</div>

															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="modal fade" id="confirm-new-test" role="dialog">
											<div class="modal-dialog modal-sm">
												<!-- Modal content no 1-->
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title text-center form-title">Поздравляем</h4>
													</div>
													<div class="modal-body padtrbl">
														<div class="login-box-body">
															<p class="login-box-msg">Новый тест был успешно
																создан и сохранен в системе</p>
															<div class="form-group">
																<form action="PrintReq" id="loginForm" method="POST">
																	<div class="form-group has-feedback"></div>
																	<div class="form-group has-feedback"></div>

																	<div class="row">
																		<div class="col-xs-12">
																			<button type="submit"
																				class="btn btn-green btn-block btn-flat">OK</button>
																		</div>
																	</div>
																</form>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>

										<div class="modal fade" id="error-new-group-1" role="dialog">
											<div class="modal-dialog modal-sm">
												<!-- Modal content no 1-->
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title text-center form-title">Ошибка</h4>
													</div>
													<div class="modal-body padtrbl">
														<div class="login-box-body">
															<p class="login-box-msg">Группа с таким названием уже
																существует в системе.</p>
															<div class="form-group">
																<form action="PrintReq" id="loginForm" method="POST">
																	<div class="form-group has-feedback"></div>
																	<div class="form-group has-feedback"></div>

																	<div class="row">
																		<div class="col-xs-12">
																			<button type="submit"
																				class="btn btn-green btn-block btn-flat">OK</button>
																		</div>
																	</div>
																</form>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="modal fade" id="error_group_2" role="dialog">
											<div class="modal-dialog modal-sm">
												<!-- Modal content no 1-->
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title text-center form-title">Ошибка</h4>
													</div>
													<div class="modal-body padtrbl">
														<div class="login-box-body">
															<p class="login-box-msg">Заполнены не все поля</p>
															<div class="form-group">
																<div class="form-group has-feedback"></div>
																<div class="form-group has-feedback"></div>
																<div class="row">
																	<div class="col-xs-12">
																		<button type="submit"
																			class="btn btn-green btn-block btn-flat"
																			data-dismiss="modal">OK</button>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="modal fade" id="confirm-new-group" role="dialog">
											<div class="modal-dialog modal-sm">
												<!-- Modal content no 1-->
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title text-center form-title">Поздравляем</h4>
													</div>
													<div class="modal-body padtrbl">
														<div class="login-box-body">
															<p class="login-box-msg">Новая группа была успешно
																создана и сохранена в системе</p>
															<div class="form-group">
																<form action="PrintReq" id="loginForm" method="POST">
																	<div class="form-group has-feedback"></div>
																	<div class="form-group has-feedback"></div>

																	<div class="row">
																		<div class="col-xs-12">
																			<button type="submit"
																				class="btn btn-green btn-block btn-flat">OK</button>
																		</div>
																	</div>
																</form>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="modal fade" id="error_appoint" role="dialog">
											<div class="modal-dialog modal-sm">
												<!-- Modal content no 1-->
												<div class="modal-content">
													<div class="modal-header">
														<button type="button" class="close" data-dismiss="modal">&times;</button>
														<h4 class="modal-title text-center form-title">Ошибка</h4>
													</div>
													<div class="modal-body padtrbl">
														<div class="login-box-body">
															<p class="login-box-msg">Выберите хотя бы одного студента или группу</p>
															<div class="form-group">
																<div class="form-group has-feedback"></div>
																<div class="form-group has-feedback"></div>
																<div class="row">
																	<div class="col-xs-12">
																		<button type="submit"
																			class="btn btn-green btn-block btn-flat"
																			data-dismiss="modal">OK</button>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<script src="js/jquery.min.js" type="text/javascript"></script>
										<script src="js/jquery.easing.min.js" type="text/javascript"></script>
										<script src="js/bootstrap.min.js" type="text/javascript"></script>
										<script src="js/custom.js" type="text/javascript"></script>
										<script src="contactform/contactform.js"
											type="text/javascript"></script>

									</div>
									<div></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>