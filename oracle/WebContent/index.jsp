<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Welcome</title>

	<link rel="stylesheet" type="text/css" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/imagehover.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/style.css">

	<script src="resources/js/jquery.min.js"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>
	<script src="resources/js/custom.js"></script>
	<script src="resources/contactform/contactform.js"></script>

</head>

<body>
	<script type="text/javascript">
		function checkIfFilled() {
			if (!$('input[name=loginid]').val() || !$('input[name=loginpsw]').val()) {
				$('#error_login').modal('show');
			} else {
				$('form#loginForm').submit();
			}
		}

		<c:if test="${requestScope['user'] == 'null'}">
			$(window).on('load',function(){
				$('#login').modal('show');
				$('#error_login_2').modal('show');
			});
		</c:if>
	</script>

	<!--Navigation bar-->
	<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#myNavbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<div class="navbar-brand">Ora<span>cle</span></div>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#" data-target="#login" data-toggle="modal">Войти</a></li>
				<li class="btn-trial"><a href="/registration_form" >Регистрация</a></li>
			</ul>
		</div>
	</div>
	</nav>
	<!--/ Navigation bar-->
	
	<!--Modal box-->
	<div class="modal fade" id="login" role="dialog">
		<div class="modal-dialog modal-sm">

			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Login</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Sign in to start your session</p>
						<div class="form-group">
							<form action="/index" id="loginForm" method="POST">
								<div class="form-group has-feedback">
									<!----- username -------------->
									<input class="form-control" placeholder="Username" name="loginid"
										type="text" autocomplete="off" /> <span
										style="display: none; font-weight: bold; position: absolute; color: red; position: absolute; padding: 4px; font-size: 11px; background-color: rgba(128, 128, 128, 0.26); z-index: 17; right: 27px; top: 5px;"
										id="span_loginid"></span>
									<!---Alredy exists  ! -->
									<span class="glyphicon glyphicon-user form-control-feedback"></span>
								</div>
								<div class="form-group has-feedback">
									<!----- password -------------->
									<input class="form-control" placeholder="Password"
										name="loginpsw" type="password" autocomplete="off" /> <span
										style="display: none; font-weight: bold; position: absolute; color: grey; position: absolute; padding: 4px; font-size: 11px; background-color: rgba(128, 128, 128, 0.26); z-index: 17; right: 27px; top: 5px;"
										id="span_loginpsw"></span>
									<!---Alredy exists  ! -->
									<span class="glyphicon glyphicon-lock form-control-feedback"></span>
								</div>
								<div class="row">
									<div class="col-xs-12">
										<div class="checkbox icheck">
											<label> <input type="checkbox" id="loginrem">
												Remember Me
											</label>
										</div>
									</div>
									<div class="col-xs-12">
										<button type="button" onclick="checkIfFilled();" class="btn btn-green btn-block btn-flat">Sign In</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/ Modal box-->

	<!--Modal box 2-->
	<div class="modal fade" id="error_login" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Ошибка входа</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Заполнены не все поля</p>
						<div class="form-group">
							<div class="form-group has-feedback">
							</div>
							<div class="form-group has-feedback">
							</div>
							<div class="row">
								<div class="col-xs-12">
									<button type="submit" class="btn btn-green btn-block btn-flat" data-dismiss="modal">OK</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	</div>
	<!--/ Modal box 2-->

	<!--Modal box 3-->
	<div class="modal fade" id="error_login_2" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Ошибка входа</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Указанного пользователя не существует в системе</p>
						<div class="form-group">
							<div class="form-group has-feedback">
							</div>
							<div class="form-group has-feedback">
							</div>
							<div class="row">
								<div class="col-xs-12">
									<button type="submit" class="btn btn-green btn-block btn-flat" data-dismiss="modal">OK</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>	</div>
	<!--/ Modal box 3-->
		
	<!--Banner-->
	<div class="banner">
		<div class="bg-color">
			<div class="container">
				<div class="row">
					<div class="banner-text text-center">
						<div class="text-border">
							<h2 class="text-dec">Trust & Quality</h2>
						</div>
						<div class="intro-para text-center quote">
							<p class="big-text">Learning Today . . . Leading Tomorrow.</p>
							<p class="small-text">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit.
								Laudantium enim repellat sapiente quos architecto<br>Laudantium
								enim repellat sapiente quos architecto
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/ Banner-->
</body>
</html>