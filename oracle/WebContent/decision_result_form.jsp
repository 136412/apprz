<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Oracle</title>

<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/imagehover.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
	<!--Navigation bar-->
	<nav class="navbar navbar-default navbar-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#myNavbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.html">Ora<span>cle</span></a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav navbar-right">
				<li class="btn-trial"><a href="#" data-target="#login"
					data-toggle="modal">Выйти из системы</a></li>
			</ul>
		</div>
	</div>
	</nav>
	<!--/ Navigation bar-->


	
	<div class="container">
		Вы зашли в систему как: <label for="name"><c:out
				value="${student_name}"></c:out></label>
	</div>
<!--  <div class="container">
		Результаты теста: <label for="name"><c:out
				value="${student_name}"></c:out></label>
	</div>-->


	<form action="PrintReq" method="POST">
		<div class="tab-content clearfix">
			<c:set var = "test_size" scope = "session" value = "${fn:length(my_test)}"/>

			<c:forEach var="task" items="${my_test}" varStatus="loopS">
		
				<c:choose>
					<c:when test="${loopS.index == 0}">
						<div class="tab-pane active" id="<c:out value="${loopS.index}"/>a">
					</c:when>
					<c:otherwise>
						<div class="tab-pane" id="<c:out value="${loopS.index}"/>a">
					</c:otherwise>
				</c:choose>
				
				<div style="text-align: center" class="container-fluid">
					<div class="row">
						<div class="col-md-4">
							<h1>
								Задача <label for="current_task"><c:out value="${loopS.index + 1}"/></label> из <label
									for="all_task"><c:out value="${test_size}"></c:out></label>
							</h1>
						</div>
						<div class="col-md">
							<button type="button" class="btn btn-green" data-toggle="tab"
								href="#<c:out value="${loopS.index - 1}"/>a">&laquo;Предыдущая</button>
							<button type="button" class="btn btn-green" data-toggle="tab"
								href="#<c:out value="${loopS.index + 1}"/>a">Следующая&raquo;</button>
						</div>
					</div>
				</div>


				<label for="description_task"><c:out value="${task.task}"></c:out></label>
				<div class="input-group">
					<p>
						<textarea readonly id="area<c:out value="${loopS.index}"/>" class="form-control" rows="10" cols="45" name="text"><c:out value="${task.solution}"></c:out></textarea>
					</p>
				</div>
				
				
				<c:out value="${task.error}" escapeXml="false"></c:out>
				
				<div class="input-group">
					<label for="comment">Ответ</label>
					<input readonly type="text" class="form-control" name="answer" value="<c:out value="${task.answer}"></c:out>">					
				</div>
			</div>
			</c:forEach>
		</div>
	
		<div class="col-md">
			<button type="button" class="btn btn-green">Вернуться</button>
		</div>
	</form>


	<script src="js/jquery.min.js" type="text/javascript"></script>
	<script src="js/jquery.easing.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/custom.js" type="text/javascript"></script>
	<script src="contactform/contactform.js" type="text/javascript"></script>

</body>
</html>