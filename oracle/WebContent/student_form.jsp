<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Oracle</title>

<link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/imagehover.min.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>

<body>
	<!--Navigation bar-->
	<nav class="navbar navbar-default navbar-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#myNavbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="index.html">Ora<span>cle</span></a>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav navbar-right">
				<li class="btn-trial"><a href="#" data-target="#login"
					data-toggle="modal">Выйти из системы</a></li>
			</ul>
		</div>
	</div>
	</nav>
	<!--/ Navigation bar-->

	<!--Modal box -->
	<div class="modal fade" id="msg" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Информация</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Текст</p>
						<div class="form-group">
							<form action="Controller" id="loginForm" method="POST">
								<div class="form-group has-feedback"></div>
								<div class="form-group has-feedback"></div>
								<div class="row">
									<div class="col-xs-12">
										<button type="submit" class="btn btn-green btn-block btn-flat">OK</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!--/ Modal box -->


	<div class="container">
		Вы зашли в систему как: <label for="name"><c:out value="${student_name}"></c:out></label>
	</div>
	<div id="exTab1" class="container">
		<ul class="nav nav-pills">
		<%
		if (request.getParameter("tabID") == null ||  request.getParameter("tabID").equals("1a")) {
		%>
			<li class="active"><a href="#1a" data-toggle="tab">Назначенные
					тесты</a></li>
			<li><a href="#2a" data-toggle="tab">Доступные тесты</a></li>
			<li><a href="#3a" data-toggle="tab">Результаты тестов</a></li>
			<li><a href="#4a" data-toggle="tab">Оценить тесты</a></li>
		<%
		} else if (request.getParameter("tabID").equals("2a")) {
		%>
			<li><a href="#1a" data-toggle="tab">Назначенные
					тесты</a></li>
			<li  class="active"><a href="#2a" data-toggle="tab">Доступные тесты</a></li>
			<li><a href="#3a" data-toggle="tab">Результаты тестов</a></li>
			<li><a href="#4a" data-toggle="tab">Оценить тесты</a></li>
		<%
		} else if (request.getParameter("tabID").equals("3a")) {
		%>
			<li><a href="#1a" data-toggle="tab">Назначенные
					тесты</a></li>
			<li><a href="#2a" data-toggle="tab">Доступные тесты</a></li>
			<li class="active"><a href="#3a" data-toggle="tab">Результаты тестов</a></li>
			<li><a href="#4a" data-toggle="tab">Оценить тесты</a></li>
		<%
		} else if (request.getParameter("tabID").equals("4a")) {
		%>
			<li><a href="#1a" data-toggle="tab">Назначенные
					тесты</a></li>
			<li><a href="#2a" data-toggle="tab">Доступные тесты</a></li>
			<li><a href="#3a" data-toggle="tab">Результаты тестов</a></li>
			<li class="active"><a href="#4a" data-toggle="tab">Оценить тесты</a></li>
		<%
		} else {
		%>
			<li><a href="#1a" data-toggle="tab">Назначенные
					тесты</a></li>
			<li><a href="#2a" data-toggle="tab">Доступные тесты</a></li>
			<li><a href="#3a" data-toggle="tab">Результаты тестов</a></li>
			<li><a href="#4a" data-toggle="tab">Оценить тесты</a></li>
		<%
		}
		%>
		</ul>

		<div class="tab-content clearfix">
		
			<%
		if (request.getParameter("tabID") == null ||  request.getParameter("tabID").equals("1a")) {
			%>
	        <div class="tab-pane active" id="1a">
	    <% } else {%>
	        <div class="tab-pane" id="1a">
	    <%}
		%>

				<!--  <form class="form-inline" action="">
					<div class="form-group">
						<p class="form-control-static">Поиск теста по его названию</p>
					</div>
					<div class="form-group">
						<input type="text" class="form-control" id="number" placeholder="">
					</div>
					<button type="submit" class="btn btn-default">Поиск</button>
				</form>  -->


				<div class="form-group">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Название</th>
								<th>Автор</th>
								<th>Тип теста</th>
								<th>Время выполения теста (в часах)</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="test" items="${assignedTests}">
								<tr>
									<td>${test.id}</td>
									<td>${test.name}</td>
									<td>${test.author}</td>
									<td>${test.type}</td>
									<td>${test.leadTime}</td>
									<td>
									<form action="PrintReq" method="POST">
										<input type="hidden" name="studentId" value="<c:out value="${student_id}"/>">
										<input type="hidden" name="testID" value="<c:out value="${test.id}" /> ">
										<button type="submit" class="btn btn-green">Начать</button>
									</form>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>

			<%
		if (request.getParameter("tabID") != null && request.getParameter("tabID").equals("2a")) {
			%>
	        <div class="tab-pane active" id="2a">
	    <% } else {%>
	        <div class="tab-pane" id="2a">
	    <%}
		%>
				

				<div class="form-group">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Название</th>
								<th>Автор</th>
								<th>Тип теста</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="test" items="${availableTests}">
								<tr>
									<td>${test.id}</td>
									<td>${test.name}</td>
									<td>${test.author}</td>
									<td>${test.type}</td>
									<c:if test="${test.type!='контрольная работа'}">
									<td>
									<form action="PrintReq" method="POST">
										<input type="hidden" name="studentId" value="<c:out value="${student_id}"/>">
										<input type="hidden" name="testID" value="<c:out value="${test.id}" /> ">
										<button type="submit" class="btn btn-green">Назначить
											на себя тест</button>
									</form>
									</td>
									</c:if>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>

			<%
		if (request.getParameter("tabID") != null && request.getParameter("tabID").equals("3a")) {
			%>
	        <div class="tab-pane active" id="3a">
	    <% } else {%>
	        <div class="tab-pane" id="3a">
	    <%}
		%>


				<div class="form-group">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Название</th>
								<th>Автор</th>
								<th>Тип теста</th>
								<th>Результаты</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="test" items="${resultsOfTheTests}">
								<tr>
									<td>${test.id}</td>
									<td>${test.name}</td>
									<td>${test.author}</td>
									<td>${test.type}</td>
									<td>${test.points}</td>
									<td>
										<form action="PrintReq" method="POST">
											<input type="hidden" name="studentId" value="<c:out value="${student_id}"/>">
											<input type="hidden" name="testID" value="<c:out value="${test.id}" /> ">
											<button type="submit" class="btn btn-green">Просмотреть
											решение</button>
										</form>
									</td>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>


			<%
		if (request.getParameter("tabID") != null && request.getParameter("tabID").equals("4a")) {
			%>
	        <div class="tab-pane active" id="4a">
	    <% } else {%>
	        <div class="tab-pane" id="4a">
	    <%}
		%>
				

				<div class="form-group">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Название</th>
								<th>Автор</th>
								<th>Тип теста</th>
								<th>Оценка</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="test" items="${resultsOfTheTests}">
								<tr>
									<td>${test.id}</td>
									<td>${test.name}</td>
									<td>${test.author}</td>
									<td>${test.type}</td>
									<form action="PrintReq" method="POST">
										<td><select class="form-control" name="test_rating">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
										</select></td>
										<td>
												<input type="hidden" name="studentId" value="<c:out value="${student_id}"/>">
												<input type="hidden" name="testID" value="<c:out value="${test.id}" /> ">
												<button type="submit" class="btn btn-green btn-space">Оценить</button>
										</td>
									</form>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>


			<%
		if (request.getParameter("tabID") != null && request.getParameter("tabID").equals("3cr")) {
			%>
	        <div class="tab-pane active" id="3cr">
	    <% } else {%>
	        <div class="tab-pane" id="3cr">
	    <%}
		%>
				<form name="" id="" class="form-horizontal" action="">
					<div class="form-group">
						<div class="container">
							Результаты студента: <label for="name">Иванов Иван
								Иванович</label>
						</div>

						<div style="text-align: center" class="container-fluid">
							<div class="row">
								<div class="col-md-4">
									<h1>
										Задача <label for="current_task">1</label> из <label
											for="all_task">34</label>
									</h1>
								</div>
								<div class="col-md">
									<nav aria-label="Page navigation">
									<ul class="pagination">
										<li><a href="#" aria-label="Previous"> <span
												aria-hidden="true">&laquo;Предыдущая</span>
										</a></li>
										<li><a href="#" aria-label="Next"><span
												aria-hidden="true">Следующая&raquo;</span> </a></li>
									</ul>
									</nav>
								</div>
							</div>
						</div>

						<label for="description_task">Текст задачи</label>


						<div class="input-group">
							<p>
								<textarea rows="10" cols="45" name="text"></textarea>
							</p>
						</div>
					</div>

					<div class="btn-toolbar" role="toolbar"
						aria-label="Toolbar with button groups">
						<div class="btn-group mr-2" role="group">
							<button type="button" class="btn btn-green btn-space"
								data-toggle="tab" href="#3a">Результаты тестов</button>

						</div>
					</div>

				</form>
			</div>


		</div>
	</div>

	<script src="js/jquery.min.js" type="text/javascript"></script>
	<script src="js/jquery.easing.min.js" type="text/javascript"></script>
	<script src="js/bootstrap.min.js" type="text/javascript"></script>
	<script src="js/custom.js" type="text/javascript"></script>
	<script src="contactform/contactform.js" type="text/javascript"></script>

</body>
</html>