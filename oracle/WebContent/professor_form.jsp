<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Oracle</title>

	<link rel="stylesheet" type="text/css" href="resources/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/imagehover.min.css">
	<link rel="stylesheet" type="text/css" href="resources/css/style.css">

	<link rel="stylesheet" href="resources/css/bootstrap.min.css">
	<link rel="stylesheet" href="resources/css/bootstrap-select.css">

	<script src="resources/js/jquery.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap-select.js" type="text/javascript"></script>

	<script src="resources/js/jquery.easing.min.js" type="text/javascript"></script>
	<script src="resources/js/custom.js" type="text/javascript"></script>
	<script src="resources/contactform/contactform.js" type="text/javascript"></script>

</head>

<body>
	<!--Navigation bar-->
	<nav class="navbar navbar-default navbar-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target="#myNavbar">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<div class="navbar-brand">Ora<span>cle</span></div>
		</div>
		<div class="collapse navbar-collapse" id="myNavbar">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="#" data-target="#creation-modal" data-toggle="modal">Создать</a></li>
				<li class="btn-trial"><a href="/index">Выйти из системы</a></li>
			</ul>
		</div>
	</div>
	</nav>
	<!--/ Navigation bar-->


	<div class="modal fade" id="select_test" role="dialog">
		<div class="modal-dialog modal-lm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-body padtrbl">
					<div class="modal-body padtrbl">

						<div class="loginmodal-container">
							<h3>Выберите, на кого хотите назначить тест</h3>
							<form action="Controller" id="new-task" method="POST">

								<div class="form-group row">
									<label class="col-sm-2">Найти</label>
									<div class="col-sm-10">
										<div class="form-check">
											<label class="form-check-label"> <input type="text"
												name="user">
											</label>
										</div>
									</div>
								</div>

								<div id="div1"
									style="height: 150px; width: 400px; position: relative; max-height: 100%; overflow: auto; border: 1px solid black;">
									<div class="itemconfiguration"
										style="padding-left: 30px; overflow-y: auto;">

										<input type="checkbox" /> Задача про яблоки<br /> <input
											type="checkbox" /> Коды Хемминга <br /> <input
											type="checkbox" /> Задача от Маркса <br /> <input
											type="checkbox" /> Задача от Маркса <br /> <input
											type="checkbox" /> Задача от Маркса <br /> <input
											type="checkbox" /> Задача от Маркса <br /> <input
											type="checkbox" /> Задача от Маркса <br /> <input
											type="checkbox" /> Задача от Маркса <br />
									</div>
								</div>
								<h3></h3>
								<div class="btn-toolbar" role="toolbar"
									aria-label="Toolbar with button groups">
									<div class="btn-group mr-2" role="group">
										<button type="button" class="btn btn-green" data-toggle="tab"
											href="#" data-target="#error-new-task">Назначить тест</button>
									</div>
									<div class="btn-group mr-2" role="group">
										<button type="button" class="btn btn-green">Отмена</button>
									</div>
								</div>

							</form>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="container">
		<div>
			Вы зашли в систему как: <label for="name">Фокин Иван Иванович</label>
		</div>
	</div>

	<div id="exTab1" class="container">
		<ul class="nav nav-pills">
			<li class="active"><a href="#1a" data-toggle="tab">Мои тесты</a></li>
			<li><a href="#2a" data-toggle="tab">Результаты тестов</a></li>
			<li><a href="#3a" data-toggle="tab">Статистика</a></li>
			<li><a href="#4a" data-toggle="tab">Задачи</a></li>
			<li><a href="#5a" data-toggle="tab">Все пользователи</a></li>
		</ul>

		<div class="tab-content clearfix">
			<div class="tab-pane active" id="1a">
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Название</th>
							<th>Тип теста</th>
							<th>Оценка теста</th>
							<th>Подробнее</th>
							<th>Назначение</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>Тест 1</td>
							<td>Учебный тест</td>
							<td>6.7</td>
							<td>
								<button type="button" class="btn btn-green" data-toggle="tab"
									href="#2c-1">Подробнее</button>
							</td>
							<td>
								<button type="button" class="btn btn-green btn-space"
									data-toggle="modal" href="#select_test">Назначить</button>
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<div class="tab-pane" id="2c-1">
				<div class="modal-body padtrbl">
					<div class="loginmodal-container">
						<form action="Controller" id="new-task" method="POST">
							<div class="form-group row">
								<label class="col-sm-2">Тест</label>
								<div class="col-sm-10">
									<div class="form-check">
										<label class="form-check-label">К/р по кодированию </label>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-2">Тип теста</label>
								<div class="col-sm-10">
									<div class="form-check">
										<label class="form-check-label">Контрольная работа </label>
									</div>
								</div>
							</div>

							Задачи в составе текста

							<table class="table">
								<thead>
									<tr>
										<th>#</th>
										<th>Название</th>
										<th>Тема</th>
										<th>Сложность</th>
										<th>Автор</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>1</td>
										<td>Яблоки</td>
										<td>Логика</td>
										<td>2</td>
										<td>Иванов Иван Иванович</td>
										<td>
											<button type="button" class="btn btn-green btn-space"
												data-toggle="tab" href="#2c-2">Подробнее</button>
										</td>
									</tr>
								</tbody>
							</table>

							<div class="btn-toolbar" role="toolbar"
								aria-label="Toolbar with button groups">
								<div class="btn-group mr-2" role="group">
									<button type="button" class="btn btn-green btn-space"
										data-toggle="tab" href="#" data-target="#1a">Список
										тестов</button>

								</div>
								<div class="btn-group mr-2" role="group">
									<button type="button" class="btn btn-green">Удалить
										тест</button>
								</div>
							</div>

						</form>
					</div>
				</div>
			</div>


			<div class="tab-pane" id="2c-2">
				<div class="modal-body padtrbl">
					<div class="loginmodal-container">
						<form action="Controller" id="new-task" method="POST">
							<div class="form-group row">
								<label class="col-sm-2">Задача</label>
								<div class="col-sm-10">
									<div class="form-check">
										<label class="form-check-label">Апельсинава роща</label>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-2">Тема задачи</label>
								<div class="col-sm-10">
									<div class="form-check">
										<label class="form-check-label">Графы</label>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-2">Сложность</label>
								<div class="col-sm-10">
									<div class="form-check">
										<label class="form-check-label">5</label>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-2">Автор</label>
								<div class="col-sm-10">
									<div class="form-check">
										<label class="form-check-label">Иванов Иван Иванович</label>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-2">Условие</label>
								<div class="col-sm-10">
									<div class="form-check">
										<label class="form-check-label">Сколько ребер в графе?</label>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-2">Изображение</label>
								<div class="col-sm-10">
									<div class="form-check">
										<label class="form-check-label"></label>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-2">Ответ</label>
								<div class="col-sm-10">
									<div class="form-check">
										<label class="form-check-label">30</label>
									</div>
								</div>
							</div>

							<div class="btn-toolbar" role="toolbar"
								aria-label="Toolbar with button groups">
								<div class="btn-group mr-2" role="group">
									<button type="button" class="btn btn-green btn-space"
										data-toggle="tab" href="#1a">Список тестов</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>


			<div class="tab-pane" id="2c-3">
				<div class="modal-body padtrbl">
					<div class="loginmodal-container">
						<form action="Controller" id="new-task" method="POST">
							<div class="form-group row">
								<label class="col-sm-2">Задача</label>
								<div class="col-sm-10">
									<div class="form-check">
										<label class="form-check-label">Апельсинава роща</label>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-2">Тема задачи</label>
								<div class="col-sm-10">
									<div class="form-check">
										<label class="form-check-label">Графы</label>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-2">Сложность</label>
								<div class="col-sm-10">
									<div class="form-check">
										<label class="form-check-label">5</label>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-2">Автор</label>
								<div class="col-sm-10">
									<div class="form-check">
										<label class="form-check-label">Иванов Иван Иванович</label>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-2">Условие</label>
								<div class="col-sm-10">
									<div class="form-check">
										<label class="form-check-label">Сколько ребер в графе?</label>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-2">Изображение</label>
								<div class="col-sm-10">
									<div class="form-check">
										<label class="form-check-label"></label>
									</div>
								</div>
							</div>

							<div class="form-group row">
								<label class="col-sm-2">Ответ</label>
								<div class="col-sm-10">
									<div class="form-check">
										<label class="form-check-label">30</label>
									</div>
								</div>
							</div>

							<div class="btn-toolbar" role="toolbar"
								aria-label="Toolbar with button groups">
								<div class="btn-group mr-2" role="group">
									<button type="button" class="btn btn-green btn-space"
										data-toggle="tab" href="#4a">Список
										задач</button>

								</div>
								<div class="btn-group mr-2" role="group">
									<button type="button" class="btn btn-green">Удалить
										задачу</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>

			<div class="tab-pane" id="2a">
				<div class="container">
					<h1></h1>
					<div class="form-group row">
						<p class="col-sm-2">Статистика по тесту</p>
						<div class="col-sm-10">
							<select id="country" name="country"
								class="form-control selectpicker">
								<option>К/р по кодированию</option>
							</select>
						</div>
					</div>

					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Студент</th>
								<th>Группа</th>
								<th>Баллы</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Иванов Иван Иванович</td>
								<td>12345/1</td>
								<td>100</td>
								<td>
									<button type="button" class="btn btn-green btn-space"
										data-toggle="tab" href="#2c-4">Просмотреть решение</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="tab-pane" id="2c-4">
				<form name="" id="" class="form-horizontal" action="">
					<div class="form-group">
						<div class="container">
							Результаты студента: <label for="name">Иванов Иван
								Иванович</label>
						</div>

						<div style="text-align: center" class="container-fluid">
							<div class="row">
								<div class="col-md-4">
									<h1>
										Задача <label for="current_task">1</label> из <label
											for="all_task">34</label>
									</h1>
								</div>
								<div class="col-md">
									<nav aria-label="Page navigation">
									<ul class="pagination">
										<li><a href="#" aria-label="Previous"> <span
												aria-hidden="true">&laquo;Предыдущая</span>
										</a></li>
										<li><a href="#" aria-label="Next"><span
												aria-hidden="true">Следующая&raquo;</span> </a></li>
									</ul>
									</nav>
								</div>
							</div>
						</div>

						<label for="description_task">Текст задачи</label>


						<div class="input-group">
							<p>
								<textarea rows="10" cols="45" name="text"></textarea>
							</p>
						</div>
					</div>

					<div class="btn-toolbar" role="toolbar"
						aria-label="Toolbar with button groups">
						<div class="btn-group mr-2" role="group">
							<button type="button" class="btn btn-green btn-space"
								data-toggle="tab" href="#2a">Список результатов</button>

						</div>
					</div>

				</form>
			</div>

			<div class="tab-pane" id="3a">
				<h1></h1>
				<div class="form-group row">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Название теста</th>
								<th>Тип теста</th>
								<th>Средний балл</th>
								<th>Минимальный балл</th>
								<th>Максимальный балл</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th scope="row">1</th>
								<td>Тест 1</td>
								<td>Учебный тест</td>
								<td>10</td>
								<td>0</td>
								<td>100</td>
								<td>
									<button type="button" class="btn btn-green btn-space"
										data-toggle="tab" href="#2c-5">Подробнее</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>

			<div class="tab-pane" id="2c-5">
				<h1></h1>
				<div class="form-group row">
					<p class="col-sm-2">Статистика по тесту</p>
					<div class="col-sm-10">
						<label for="name" class="form-check-label">К/р по
							кодированию</label>
					</div>
				</div>

				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Студент</th>
							<th>Группа</th>
							<th>Количество попыток</th>
							<th>Первая попытка</th>
							<th>Последняя попытка</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row">1</th>
							<td>Иванов Иван Иванович</td>
							<td>12345/1</td>
							<td>10</td>
							<td>0</td>
							<td>100</td>
						</tr>
					</tbody>
				</table>

				<div class="btn-toolbar" role="toolbar"
					aria-label="Toolbar with button groups">
					<div class="btn-group mr-2" role="group">
						<button type="button" class="btn btn-green btn-space"
							data-toggle="tab" href="#3a">Статистика</button>
					</div>
				</div>
			</div>

			<div class="tab-pane" id="4a">
				<form name="" id="" class="form-horizontal" action="">
					<div class="form-group">
						<div class="col-sm-3">
							<div class="btn-group" data-toggle="buttons">
								<label class="btn btn-primary active"> <input
									type="radio" name="options" id="option1" autocomplete="off"
									checked> Мои задачи
								</label> <label class="btn btn-primary"> <input type="radio"
									name="options" id="option2" autocomplete="off"> Все
									задачи
								</label>
							</div>
						</div>
						<div class="col-sm-3">
							<p class="form-control-static">Поиск задачи по его названию</p>
						</div>
						<div class="col-sm-3">
							<label for="lastname" class="sr-only"></label> <input
								id="lastname" class="form-control input-group-lg reg_name"
								type="text" name="lastname" title="Enter last name">
						</div>
						<div class="col-sm-3">
							<button type="submit" class="btn btn-default">Поиск</button>
						</div>
					</div>
				</form>

				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>Название</th>
							<th>Тема</th>
							<th>Сложность</th>
							<th>Автор</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row">1</th>
							<td>Яблоки</td>
							<td>Арифметика</td>
							<td>1</td>
							<td>Иванов Иван Иванович</td>
							<td>
								<button type="button" class="btn btn-green btn-space"
									data-toggle="tab" href="#2c-3">Подробнее</button>
							</td>

						</tr>
					</tbody>
				</table>
			</div>


			<div class="tab-pane" id="5a">
				<form name="" id="" class="form-horizontal" action="">
					<div class="form-group">
						<div class="col-sm-5">
							<div class="btn-group" data-toggle="buttons">
								<label class="btn btn-primary active"> <input
									type="radio" name="options" id="option1" autocomplete="off"
									checked> Только преподаватели
								</label> <label class="btn btn-primary"> <input type="radio"
									name="options" id="option2" autocomplete="off"> Только
									студенты
								</label>
							</div>
						</div>
					</div>
				</form>

				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>ФИО</th>
							<th>Тип пользователя</th>
							<th>Группа</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="row">1</th>
							<td>Иванов Иван Иванович</td>
							<td>Студент</td>
							<td>12345/2</td>
						</tr>
					</tbody>
				</table>
			</div>

		</div>

		<div class="modal fade" id="creation-modal" role="dialog">
			<div class="modal-dialog modal-lm">
				<!-- Modal content no 1-->
				<div class="modal-content">

					<div class="modal-body padtrbl">
						<div id="exTab1" class="container">
							<ul class="nav nav-pills">
								<li class="active"><a href="#1b" data-toggle="tab">Задача</a></li>
								<li><a href="#2b" data-toggle="tab">Тест</a></li>
								<li><a href="#3b" data-toggle="tab">Группа студентов</a></li>
							</ul>

							<div class="tab-content clearfix">
								<div class="tab-pane active" id="1b">
									<div class="modal-body padtrbl">

										<div class="loginmodal-container">
											<h3>Новая задача</h3>
											<form action="Controller" id="new-task" method="POST">

												<div class="form-group row">
													<label class="col-sm-2">название</label>
													<div class="col-sm-10">
														<div class="form-check">
															<label class="form-check-label"> <input
																type="text" name="user">
															</label>
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-2">тема</label>
													<div class="col-sm-10">
														<div class="form-check">
															<label class="form-check-label"> <select
																class="form-control">
																	<option value="one">Комбинаторика</option>
																	<option value="two">Теория множеств</option>
																	<option value="three">Булева алгебра</option>
															</select>
															</label>
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-2">сложность</label>
													<div class="col-sm-10">
														<div class="form-check">
															<label class="form-check-label"> <select
																class="form-control">
																	<option value="one">1</option>
																	<option value="two">2</option>
																	<option value="three">3</option>
																	<option value="four">4</option>
																	<option value="five">5</option>
															</select>
															</label>
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-2">условие</label>
													<div class="col-sm-10">
														<div class="form-check">
															<label class="form-check-label"> <input
																type="text" name="user">
															</label>
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-2">изображение</label>
													<div class="col-sm-10">
														<div class="form-check">
															<label class="form-check-label">
																<button type="button" class="btn btn-green">Загрузить</button>
															</label>
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-2">правильный ответ</label>
													<div class="col-sm-10">
														<div class="form-check">
															<label class="form-check-label"> <input
																type="text" name="user">
															</label>
														</div>
													</div>
												</div>
												<div class="btn-toolbar" role="toolbar"
													aria-label="Toolbar with button groups">
													<div class="btn-group mr-2" role="group">
														<button type="button" class="btn btn-green"
															data-toggle="modal" href="#"
															data-target="#error-new-task">Создать задачу</button>
													</div>
													<div class="btn-group mr-2" role="group">
														<button type="button" class="btn btn-green">Отмена</button>
													</div>
												</div>
											</form>

										</div>
									</div>
								</div>

								<div class="tab-pane" id="2b">
									<div class="modal-body padtrbl">

										<div class="loginmodal-container">
											<h3>Новый тест</h3>
											<form action="Controller" id="new-task" method="POST">

												<div class="form-group row">
													<label class="col-sm-2">название</label>
													<div class="col-sm-10">
														<div class="form-check">
															<label class="form-check-label"> <input
																type="text" name="user">
															</label>
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-2">тип теста</label>
													<div class="col-sm-10">
														<div class="form-check">
															<label class="form-check-label"> <select
																class="form-control">
																	<option value="one">учебный тест</option>
																	<option value="two">контрольная работа</option>
															</select>
															</label>
														</div>
													</div>
												</div>

												<div class="form-group row">
													<label class="col-sm-2">время работы</label>
													<div class="col-sm-10">
														<div class="form-check">
															<label class="form-check-label"> <input
																type="text" name="user">
															</label>
														</div>
													</div>
												</div>

												<div class="btn-toolbar" role="toolbar"
													aria-label="Toolbar with button groups">
													<div class="btn-group mr-2" role="group">
														<button type="button" class="btn btn-green"
															data-toggle="tab" href="#2b-1">Выбрать задачи</button>
													</div>
													<div class="btn-group mr-2" role="group">
														<button type="button" class="btn btn-green">Отмена</button>
													</div>
												</div>
											</form>

										</div>
									</div>
								</div>
								<div class="tab-pane" id="2b-1">
									<div class="modal-body padtrbl">

										<div class="loginmodal-container">
											<h3>Выберите задачи из списка</h3>
											<form action="Controller" id="new-task" method="POST">

												<div class="form-group row">
													<label class="col-sm-2">найти</label>
													<div class="col-sm-10">
														<div class="form-check">
															<label class="form-check-label"> <input
																type="text" name="user">
															</label>
														</div>
													</div>
												</div>
												<div id="div1"
													style="height: 150px; width: 400px; position: relative; max-height: 100%; overflow: auto; border: 1px solid black;">

													<div class="itemconfiguration"
														style="padding-left: 30px; overflow-y: auto;">

														<input type="checkbox" /> Задача про яблоки<br /> <input
															type="checkbox" /> Коды Хемминга <br /> <input
															type="checkbox" /> Задача от Маркса <br /> <input
															type="checkbox" /> Задача от Маркса <br /> <input
															type="checkbox" /> Задача от Маркса <br /> <input
															type="checkbox" /> Задача от Маркса <br /> <input
															type="checkbox" /> Задача от Маркса <br /> <input
															type="checkbox" /> Задача от Маркса <br />
													</div>
												</div>
												<h3></h3>
												<div class="btn-toolbar" role="toolbar"
													aria-label="Toolbar with button groups">
													<div class="btn-group mr-2" role="group">
														<button type="button" class="btn btn-green">Создать
															тест</button>
													</div>
													<div class="btn-group mr-2" role="group">
														<button type="button" class="btn btn-green">Отмена</button>
													</div>
												</div>


											</form>

										</div>
									</div>
								</div>
								<div class="tab-pane" id="3b">
									<div class="modal-body padtrbl">

										<div class="loginmodal-container">
											<h3>Новая группа студентов</h3>
											<form action="Controller" id="new-task" method="POST">

												<div class="form-group row">
													<label class="col-sm-2">название</label>
													<div class="col-sm-10">
														<div class="form-check">
															<label class="form-check-label"> <input
																type="text" name="user">
															</label>
														</div>
													</div>
												</div>
												<div class="form-group row">
													<label class="col-sm-2">участники</label>
												</div>
												<div class="form-group row">
													<label class="col-sm-2">найти</label>
													<div class="col-sm-10">
														<div class="form-check">
															<label class="form-check-label"> <input
																type="text" name="user">
															</label>
														</div>
													</div>
												</div>
												<div id="div1"
													style="height: 150px; width: 400px; position: relative; max-height: 100%; overflow: auto; border: 1px solid black;">

													<div class="itemconfiguration"
														style="padding-left: 30px; overflow-y: auto;">

														<input type="checkbox" /> Илья<br /> <input
															type="checkbox" /> Федя <br /> <input type="checkbox" />
														Лили <br /> <input type="checkbox" /> Дарья <br /> <input
															type="checkbox" /> Гриша <br /> <input type="checkbox" />
														Наташа <br /> <input type="checkbox" /> Никита <br />
													</div>
												</div>
												<h3></h3>
												<div class="btn-toolbar" role="toolbar"
													aria-label="Toolbar with button groups">
													<div class="btn-group mr-2" role="group">
														<button type="button" class="btn btn-green">Создать
															группу</button>
													</div>
													<div class="btn-group mr-2" role="group">
														<button type="button" class="btn btn-green">Отмена</button>
													</div>
												</div>


											</form>

										</div>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="error-new-task" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Ошибка</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Все поля обязательны для заполнения</p>
						<div class="form-group">
							<form action="Controller" id="loginForm" method="POST">
								<div class="form-group has-feedback"></div>
								<div class="form-group has-feedback"></div>

								<div class="row">
									<div class="col-xs-12">
										<button type="submit" class="btn btn-green btn-block btn-flat">OK</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>	</div>
	<div class="modal fade" id="confirm-new-task" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Поздравляем</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Новая задача была создана и успешно
							сохранена в системе</p>
						<div class="form-group">
							<form action="Controller" id="loginForm" method="POST">
								<div class="form-group has-feedback"></div>
								<div class="form-group has-feedback"></div>

								<div class="row">
									<div class="col-xs-12">
										<button type="submit" class="btn btn-green btn-block btn-flat">OK</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="error-new-test-1" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Ошибка</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Тест с таким названием уже существует в системе.</p>
						<div class="form-group">
							<form action="Controller" id="loginForm" method="POST">
								<div class="form-group has-feedback"></div>
								<div class="form-group has-feedback"></div>

								<div class="row">
									<div class="col-xs-12">
										<button type="submit" class="btn btn-green btn-block btn-flat">OK</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="error-new-test-2" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Ошибка</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Заполнены не все поля</p>
						<div class="form-group">
							<form action="Controller" id="loginForm" method="POST">
								<div class="form-group has-feedback"></div>
								<div class="form-group has-feedback"></div>

								<div class="row">
									<div class="col-xs-12">
										<button type="submit" class="btn btn-green btn-block btn-flat">OK</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="confirm-new-test" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Поздравляем</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Новый тест был успешно создан и
							сохранен в системе</p>
						<div class="form-group">
							<form action="Controller" id="loginForm" method="POST">
								<div class="form-group has-feedback"></div>
								<div class="form-group has-feedback"></div>

								<div class="row">
									<div class="col-xs-12">
										<button type="submit" class="btn btn-green btn-block btn-flat">OK</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade" id="error-new-group-1" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Ошибка</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Группа с таким названием уже существует в системе.</p>
						<div class="form-group">
							<form action="Controller" id="loginForm" method="POST">
								<div class="form-group has-feedback"></div>
								<div class="form-group has-feedback"></div>

								<div class="row">
									<div class="col-xs-12">
										<button type="submit" class="btn btn-green btn-block btn-flat">OK</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="error-new-group-2" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Ошибка</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Заполнены не все поля</p>
						<div class="form-group">
							<form action="Controller" id="loginForm" method="POST">
								<div class="form-group has-feedback"></div>
								<div class="form-group has-feedback"></div>

								<div class="row">
									<div class="col-xs-12">
										<button type="submit" class="btn btn-green btn-block btn-flat">OK</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="confirm-new-group" role="dialog">
		<div class="modal-dialog modal-sm">
			<!-- Modal content no 1-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title text-center form-title">Поздравляем</h4>
				</div>
				<div class="modal-body padtrbl">
					<div class="login-box-body">
						<p class="login-box-msg">Новая группа была успешно создана и
							сохранена в системе</p>
						<div class="form-group">
							<form action="Controller" id="loginForm" method="POST">
								<div class="form-group has-feedback"></div>
								<div class="form-group has-feedback"></div>

								<div class="row">
									<div class="col-xs-12">
										<button type="submit" class="btn btn-green btn-block btn-flat">OK</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	</div>
	<div></div>
</body>
</html>